/**
 * Coalesced memory visit benchmark
 */

#include <stdio.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <getopt.h>

const  int THREADSPERBLOCK = 512;

void cuda_timer_start(cudaEvent_t start){
    cudaEventRecord(start);
}

void cuda_timer_stop(cudaEvent_t start, cudaEvent_t stop, float &mili){
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&mili, start, stop);
    cudaDeviceSynchronize();
}

__global__ void coalesced(int flag, int numElements, float *in) {
  //if continues in x demension
  if(flag == 0) {
    int gIdx = threadIdx.x + threadIdx.y * blockDim.x + 
      blockIdx.x * THREADSPERBLOCK;
    if(gIdx < numElements) {
      float tmp = in[gIdx];
      tmp += 1;
      in[gIdx] = tmp;
    }
  }
  else {
    int gIdy = threadIdx.y + threadIdx.x * blockDim.y + 
      blockIdx.x * THREADSPERBLOCK;
    if(gIdy < numElements) {
      float tmp = in[gIdy];
      tmp += 1;
      in[gIdy] = tmp;
    }
  }
}


int
main(int argc, char ** argv)
{
  // Error code to check return values for CUDA calls
  cudaError_t err = cudaSuccess;
  //  length of array
  int numElements = 1400 * 1024 * 1024;
  size_t size = numElements * sizeof(float);
  float *h_A = (float *)malloc(size);
  if (h_A == NULL) {
      fprintf(stderr, "Failed to allocate host vectors!\n");
      exit(EXIT_FAILURE);
  }
  // Initialize the host input vectors
  for (int i = 0; i < numElements; ++i){
      h_A[i] = rand()/(float)RAND_MAX;
  }

  // Allocate the device input vector A
  float *d_A = NULL;
  err = cudaMalloc((void **)&d_A, size);
  if (err != cudaSuccess){
      fprintf(stderr, "Allocate device memory error, error code %s!\n", cudaGetErrorString(err));
      exit(EXIT_FAILURE);
  }

  err = cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
  if (err != cudaSuccess){
      fprintf(stderr, "H2D error, error code %s)!\n", cudaGetErrorString(err));
      exit(EXIT_FAILURE);
  }

  // Launch the benchmarkernel
  int blocksPerGrid =(numElements + THREADSPERBLOCK - 1) / THREADSPERBLOCK;
  printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, THREADSPERBLOCK);
  dim3 dimBlock(32, 32);
  //warm up
  coalesced<<<blocksPerGrid, dimBlock>>>(1, numElements, d_A);

  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);    
  float microsecond;
  cuda_timer_start(start);
  for(int i = 0; i < 100; i++) {
    coalesced<<<blocksPerGrid, dimBlock>>>(0, numElements, d_A);
  }
  cuda_timer_stop(start, stop, microsecond);
  printf("visit in threadIdx.x dimension time:%f\n", microsecond / 100);
  err = cudaGetLastError();
  if (err != cudaSuccess){
      fprintf(stderr, "kernel error code %s)!\n", cudaGetErrorString(err));
  }

  cuda_timer_start(start);
  for(int i = 0; i < 100; i++) {
    coalesced<<<blocksPerGrid, dimBlock>>>(1, numElements, d_A);
  }
  cuda_timer_stop(start, stop, microsecond);
  printf("visit in threadIdx.y dimension time:%f\n", microsecond / 100);


  // Free device global memory
  err = cudaFree(d_A);
  if (err != cudaSuccess){
      fprintf(stderr, "free device memory error code %s!\n", cudaGetErrorString(err));
      exit(EXIT_FAILURE);
  }

  // Free host memory
  free(h_A);

  return 0;
}

