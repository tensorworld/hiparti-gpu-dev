/**
 * AtomicAdd overhead benchmark
 */

#include <stdio.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <getopt.h>

const  int THREADSPERBLOCK = 256;
void print_usage() {
  printf("./AtomicTest nthreads kernel");
}

void cuda_timer_start(cudaEvent_t start){
    cudaEventRecord(start);
}
void cuda_timer_stop(cudaEvent_t start, cudaEvent_t stop, float &mili){
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&mili, start, stop);
    cudaDeviceSynchronize();
}

__global__ void
testAtomicAdd(float *C, int numElements, int contention_num)
{
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  float tmp = i * 2.0 / numElements;
  if (i < numElements){
    int j = i & contention_num; 
    //int j = i >> contention_num; 
    atomicAdd(&C[j], tmp);
  }
}

__global__ void
testNormalAdd(float *C, int numElements, int contention_num) {
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  float tmp = i * 2.0 / numElements;
  if (i < numElements){
    int j = i & contention_num;
    //int j = i >> contention_num;
    C[j] += tmp;
  }
}


__global__ void
testAtomicAddSM(float *C, int numElements, int contention_num) {
  __shared__ float tmp[THREADSPERBLOCK];
  int lid = threadIdx.x;
  int gid = blockDim.x * blockIdx.x + threadIdx.x;
  float x = gid * 2.0 / numElements;
  if(gid < numElements) {
    tmp[lid] = C[gid];
    int lj = lid & contention_num;
    int gj = gid & contention_num;
    //int lj = lid >> contention_num;
    //int gj = gid >> contention_num;
    //sum in shared memory
    //__syncthreads()
    atomicAdd(&tmp[lj], x);
    //the thread owns the value write to global
    if(lid == lj)
      C[gj] += tmp[lj];
  }
}

__global__ void 
testNormalAddSM(float *C, int numElements, int contention_num) {
  __shared__ float tmp[THREADSPERBLOCK];
  int lid = threadIdx.x;
  int gid = blockDim.x * blockIdx.x + threadIdx.x;
  float x = gid * 2.0 / (gridDim.x * blockDim.x);
  if(gid < numElements) {
    tmp[lid] = C[gid];
    int lj = lid & contention_num;
    int gj = gid & contention_num;
    //int lj = lid & contention_num;
    //int gj = gid & contention_num;
    //sum in shared memory
    tmp[lj] += x;
    //the thread owns the value write to global
    if(lid == lj)
      C[gj] += tmp[lj];
  }
}

typedef void (*BenchKernel)(float *, int, int);
BenchKernel kernels[] = {testNormalAdd, testAtomicAdd, testNormalAddSM, testAtomicAddSM};
int
main(int argc, char ** argv)
{
  if(argc != 3) {
    print_usage();
    return -1;
  }
  int kid = atoi(argv[1]);
  int contention_num = atoi(argv[2]);
  BenchKernel bench_kernel = kernels[kid];
  contention_num = (-1 >> contention_num) << contention_num;
  printf("kid %d, contention_num %x\n", kid, contention_num);
  // Error code to check return values for CUDA calls
  cudaError_t err = cudaSuccess;
  //  length of array
  int numElements = 140 * 1024 * 1024;
  size_t size = numElements * sizeof(float);
  // Allocate the host output vector C
  float *h_A = (float *)malloc(size);
  if (h_A == NULL)
  {
      fprintf(stderr, "Failed to allocate host vectors!\n");
      exit(EXIT_FAILURE);
  }
  // Initialize the host input vectors
  for (int i = 0; i < numElements; ++i){
      h_A[i] = rand()/(float)RAND_MAX;
  }

  // Allocate the device input vector A
  float *d_A = NULL;
  err = cudaMalloc((void **)&d_A, size);
  if (err != cudaSuccess){
      fprintf(stderr, "Allocate device memory error, error code %s!\n", cudaGetErrorString(err));
      exit(EXIT_FAILURE);
  }

  err = cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
  if (err != cudaSuccess){
      fprintf(stderr, "H2D error, error code %s)!\n", cudaGetErrorString(err));
      exit(EXIT_FAILURE);
  }

  // Launch the benchmarkernel
  int blocksPerGrid =(numElements + THREADSPERBLOCK - 1) / THREADSPERBLOCK;
  printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, THREADSPERBLOCK);
  //warm up
  for(int i = 0; i < 50; i++) {
    bench_kernel<<<blocksPerGrid, THREADSPERBLOCK>>>(d_A, numElements,contention_num);
  }
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);    
  float sum = 0;
  float microsecond;
  for(int i = 0; i < 100; i++) {
    cuda_timer_start(start);
    bench_kernel<<<blocksPerGrid, THREADSPERBLOCK>>>(d_A, numElements, contention_num);
    cuda_timer_stop(start, stop, microsecond);
    sum += microsecond;
  }
  printf("time:%f\n", sum / 100);
  err = cudaGetLastError();
  if (err != cudaSuccess){
      fprintf(stderr, "kernel error code %s)!\n", cudaGetErrorString(err));
  }

  err = cudaMemcpy(h_A, d_A, size, cudaMemcpyDeviceToHost);
  if (err != cudaSuccess){
      fprintf(stderr, "D2H error code %s!\n", cudaGetErrorString(err));
      exit(EXIT_FAILURE);
  }

  // Free device global memory
  err = cudaFree(d_A);
  if (err != cudaSuccess){
      fprintf(stderr, "free device memory error code %s!\n", cudaGetErrorString(err));
      exit(EXIT_FAILURE);
  }

  // Free host memory
  free(h_A);

  return 0;
}

