#!/bin/bash

set -e

meson configure build -Db_coverage=true
ninja -C build test
ninja -C build coverage-html
