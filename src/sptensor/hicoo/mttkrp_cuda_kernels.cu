/*
    This file is part of ParTI!.

    ParTI! is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    ParTI! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with ParTI!.
    If not, see <http://www.gnu.org/licenses/>.
*/

#include <ParTI.h>
#include "hicoo.h"
#include "mttkrp_cuda_kernels.h"
#include <inttypes.h>
#define MIN(x, y) (x < y ? x : y)
#define ONE_32 (sptIndex)1 
sptIndex ONE = 1;
typedef void (*MTTKRPHICOOCUDAKERNEL)( 
    const sptIndex, 
    const sptIndex,
    const sptNnzIndex,
    const sptIndex,
    const sptIndex,
    const sptElementIndex,
    const sptElementIndex,
    const sptNnzIndex,
    const sptNnzIndex,
    const sptNnzIndex,
    sptIndex * const,
    sptNnzIndex * const,
    sptNnzIndex * const,
    sptBlockIndex ** const,
    sptElementIndex ** const,
    sptValue * const ,
    sptIndex * const ,
    sptValue ** const);
typedef void (*MTTKRPHICOOCUDAKERNELGPU)( 
    const sptIndex, 
    const sptIndex,
    const sptNnzIndex,
    const sptIndex,
    const sptIndex,
    const sptIndex,
    const sptElementIndex,
    const sptNnzIndex,
    const sptNnzIndex,
    const sptNnzIndex,
    sptIndex * const,
    sptNnzIndex * const,
    sptNnzIndex * const,
    sptNnzIndex * const,
    sptIndex * const,
    sptValue * const ,
    sptIndex * const ,
    sptValue ** const);
//support only 3D tensors now
int sptMTTKRPKernelHiCOO(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptNnzIndex max_nnzb,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptIndex blength,
    const int impl_num,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    int result = 0;

    //for(int i = 0; i < nnz; i++) {
    //  printf("%f ", dev_values[i]);
    //}
    printf("sb: %u\n", sb_bits);
    /* Maximum settings */
    //sptIndex max_nthreads_per_block = 256;
    sptIndex max_nblocks = 32768;
    sptIndex max_R = 4;

    sptIndex nthreadsx = 0;
    sptIndex nthreadsy = 0;
    sptIndex nblocks = 0;
    sptIndex shr_size = 0;
    sptNnzIndex all_nblocks = blength;  //bptr.len-1 
    sptNnzIndex all_chunks = kptr_begin; //cptr.len / 2
    cudaError_t err;
    MTTKRPHICOOCUDAKERNEL kernel = NULL;
    sptAssert(nmodes == 3); //support only 3d tensors for now
    switch(impl_num) {
    case 1: // Naive, 1D
        nthreadsx = max_nnzb;
        sptAssert(nthreadsx < 1024);
        nblocks = MIN(all_nblocks, max_nblocks);
        kernel = spt_MTTKRPKernelHiCOO_3D_naive;
        break;
    case 2:
        nthreadsy = R;
        nthreadsx = max_nnzb;
        sptAssert(nthreadsx * nthreadsy < 1024);
        nblocks = MIN(all_nblocks, max_nblocks);
        kernel = spt_MTTKRPKernelRankHiCOO_3D_naive;
        break;
    case 3:
        nthreadsx = R;
        nthreadsy = max_nnzb;
        sptAssert(nthreadsx * nthreadsy < 1024);
        nblocks = MIN(all_nblocks, max_nblocks);
        kernel = spt_MTTKRPKernelRankSplitHiCOO_3D_naive;
        break;
    case 4:
        nthreadsx = MIN(R, max_R);
        nthreadsy = max_nnzb;
        nblocks = MIN(all_nblocks, max_nblocks);
        kernel = spt_MTTKRPKernelRankSplitHiCOORB_3D_naive;
        break;
    /* Matrix blocked implementations */
    case 14:
        nthreadsx = MIN(R, max_R);
        nthreadsy = max_nnzb;
        nblocks = MIN(all_nblocks, max_nblocks);
        kernel = spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked;
        break;
    /* configurable block*/
    case 102:
        printf("called kernel 102\n");
        nthreadsx = 64;
        nthreadsy = 0;
        nblocks = MIN(all_nblocks, max_nblocks);
        kernel = spt_MTTKRPKernelRankHiCOO_3D_naive_new;
        break;
    case 103:
        fprintf(stdout, "called kernel 103\n");
        nthreadsx = 128;
        nthreadsy = 0;
        nblocks = MIN(all_chunks, max_nblocks);
        kernel = sptMTTKRPHiCOOKernelOptimizedEinds3D;
        break;
    case 104:
        fprintf(stdout, "called kernel 104\n");
        nthreadsx = 256;
        nthreadsy = 0;
        nblocks = MIN(all_chunks, max_nblocks);
        kernel = spt_MTTKRPKernelRankHiCOO_3D_chunk;
        break;
    case 105:
        fprintf(stdout, "called kernel 105\n");
        nthreadsx = 128;
        nthreadsy = 0;
        nblocks = MIN(all_chunks, max_nblocks);
        kernel = sptMTTKRPHiCOOKernelCombinedInds3D;
        break;
    /* Shared memory for the output matrix + switch block sizes */
    case 15:
        nthreadsx = MIN(R, max_R);
        nthreadsy = max_nnzb;
        nblocks = MIN(all_nblocks, max_nblocks);
        shr_size = (sptIndex)pow(2, sb_bits) * R * sizeof(sptValue);
        kernel = spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked_SM;
        break;
    /* Shared memory for three matrices + switch block sizes */
    case 16:
        nthreadsx = MIN(R, max_R);
        nthreadsy = max_nnzb;
        nblocks = MIN(all_nblocks, max_nblocks);
        shr_size = nmodes * (sptIndex)pow(2, sb_bits) * R * sizeof(sptValue);
        kernel = spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked_AllSM;
        break;
    }

    if(nthreadsy == 0) {
       //printf("calling right\n");
       (*kernel)<<<nblocks, nthreadsx>>>(
           mode,
           nmodes,
           nnz,
           R,
           stride,
           sb_bits,
           sc_bits,
           blength,
           kptr_begin,
           kptr_end,
           dev_ndims,
           dev_cptr,
           dev_bptr,
           dev_binds,
           dev_einds,
           dev_values,
           dev_mats_order,
           dev_mats);
         
    }
    else {
      //printf("calling error \n");
      dim3 dimBlock(nthreadsx, nthreadsy);
      (*kernel)<<<nblocks, dimBlock>>>(
          mode,
          nmodes,
          nnz,
          R,
          stride,
          sb_bits,
          sc_bits,
          blength,
          kptr_begin,
          kptr_end,
          dev_ndims,
          dev_cptr,
          dev_bptr,
          dev_binds,
          dev_einds,
          dev_values,
          dev_mats_order,
          dev_mats);
    }
    err = cudaGetLastError();
    if(err != 0)
      printf("error %s in kernel %d\n",cudaGetErrorString(err), impl_num);
    result = cudaDeviceSynchronize();
    spt_CheckCudaError(result != 0, "CUDA HiCOO SpTns MTTKRP");
    printf("\nconfiguration, %6u, %3u, %3u, SM, %5u bytes\n", 
        nblocks, nthreadsx, nthreadsy, shr_size);
    return 0;
}

int sptMTTKRPKernelHiCOOGPU(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptNnzIndex max_nnzb,
    const sptIndex R,
    const sptIndex stride,
    const sptIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptIndex blength,
    const int impl_num,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptNnzIndex * const dev_binds,
    sptIndex * const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    int result = 0;
    printf("\n");

    sptIndex max_nblocks = 32768;

    sptIndex nthreadsx = 0;
    sptIndex nthreadsy = 0;
    sptIndex nblocks = 0;
    sptIndex shr_size = 0;
    sptNnzIndex all_nblocks = blength;  //bptr.len-1 
    sptNnzIndex all_chunks = kptr_begin; //cptr.len / 2
    cudaError_t err;
    MTTKRPHICOOCUDAKERNELGPU kernel = NULL;
    sptAssert(nmodes == 3); //support only 3d tensors for now
    switch(impl_num) {
    /* configurable block*/
    case 106:
        fprintf(stdout, "called kernel 106\n");
        nthreadsx = 128;
        nthreadsy = 0;
        nblocks = MIN(all_chunks, max_nblocks);
        kernel = sptMTTKRPHiCOOKernelCombinedInds3DGPU;
        break;
    }

    //printf("calling right\n");
    (*kernel)<<<nblocks, nthreadsx>>>(
        mode,
        nmodes,
        nnz,
        R,
        stride,
        sb_bits,
        sc_bits,
        blength,
        kptr_begin,
        kptr_end,
        dev_ndims,
        dev_cptr,
        dev_bptr,
        dev_binds,
        dev_einds,
        dev_values,
        dev_mats_order,
        dev_mats);
    err = cudaGetLastError();
    if(err != 0)
      printf("error %s in kernel %d\n",cudaGetErrorString(err), impl_num);
    result = cudaDeviceSynchronize();
    spt_CheckCudaError(result != 0, "CUDA HiCOO SpTns MTTKRP");
    printf("\nconfiguration, %6u, %3u, %3u, SM, %5u bytes\n", 
        nblocks, nthreadsx, nthreadsy, shr_size);
    return 0;
}

/* impl_num = 01  Naive, 1-D 
 * Limitation: blockDim.x (max_nnz) <= 1024.
 */
__global__ void spt_MTTKRPKernelHiCOO_3D_naive(
  const sptIndex mode,
  const sptIndex nmodes,
  const sptNnzIndex nnz,
  const sptIndex R,
  const sptIndex stride,
  const sptElementIndex sb_bits,
  const sptElementIndex sc_bits,
  const sptNnzIndex blength,
  const sptNnzIndex kptr_begin,
  const sptNnzIndex kptr_end,
  sptIndex * const dev_ndims,
  sptNnzIndex * const dev_cptr,
  sptNnzIndex * const dev_bptr,
  sptBlockIndex ** const dev_binds,
  sptElementIndex ** const dev_einds,
  sptValue * const dev_values,
  sptIndex * const dev_mats_order,
  sptValue ** const dev_mats)
{
  sptNnzIndex const all_nblocks = blength;
  const sptIndex tidx = threadIdx.x;
  sptNnzIndex z;
  sptIndex block_coord_mode, block_coord_1, block_coord_2;

  sptValue * const mvals = dev_mats[nmodes];
  sptIndex const times_mat_index_1 = dev_mats_order[1];
  sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
  sptIndex const times_mat_index_2 = dev_mats_order[2];
  sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

  sptNnzIndex num_loops_blocks = 1;
  if(all_nblocks > gridDim.x) {
    num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
  }
  for(sptNnzIndex nb = 0; nb < num_loops_blocks; ++nb) {
    /* Block level */
    sptNnzIndex b = blockIdx.x + nb * gridDim.x;
    if(b < blength) {
      /* Block indices */
      block_coord_mode = dev_binds[mode][b] << sb_bits;
      block_coord_1 = dev_binds[times_mat_index_1][b] << sb_bits;
      block_coord_2 = dev_binds[times_mat_index_2][b] << sb_bits;

      /* TODO: duplicated in registers */
      sptNnzIndex const bptr_begin = dev_bptr[b];
      sptNnzIndex const bptr_end = dev_bptr[b+1];

      /* Thread level */
      z = tidx + bptr_begin;
      if(z < bptr_end) {
          sptValue const entry = dev_values[z];
          //printf("%f\n", entry);
          sptNnzIndex const mode_i = block_coord_mode + dev_einds[mode][z];
          sptNnzIndex const tmp_i_1 = block_coord_1 + 
            dev_einds[times_mat_index_1][z];
          sptNnzIndex const tmp_i_2 = block_coord_2 + 
            dev_einds[times_mat_index_2][z];

          sptValue tmp_val = 0;
          for(sptIndex r=0; r<R; ++r) {
              tmp_val = entry * times_mat_1[tmp_i_1 * stride + r] * 
                times_mat_2[tmp_i_2 * stride + r];
              atomicAdd(&(mvals[mode_i * stride + r]), tmp_val);
          }
      }   // End loop entries
    }
  }   // End loop blocks
}

/* impl_num = 02  Naive, 2-D 
 * Limitation: blockDim.x (max_nnz) * R <= 1024.
 */
__global__ void spt_MTTKRPKernelRankHiCOO_3D_naive(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    sptNnzIndex const all_nblocks = blength;
    const sptIndex tidx = threadIdx.x;
    const sptIndex tidy = threadIdx.y;
    sptNnzIndex z;
    sptIndex block_coord_mode, block_coord_1, block_coord_2;

    sptValue * const mvals = dev_mats[nmodes];
    sptIndex const times_mat_index_1 = dev_mats_order[1];
    sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
    sptIndex const times_mat_index_2 = dev_mats_order[2];
    sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

    sptNnzIndex num_loops_blocks = 1;
    if(all_nblocks > gridDim.x) {
        num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
    }
    for(sptNnzIndex nb=0; nb<num_loops_blocks; ++nb) {
        /* Block level */
        sptNnzIndex b = blockIdx.x + nb * gridDim.x;
        if(b < blength) {
            /* Block indices */
            block_coord_mode = dev_binds[mode][b] << sb_bits;
            block_coord_1 = dev_binds[times_mat_index_1][b] << sb_bits;
            block_coord_2 = dev_binds[times_mat_index_2][b] << sb_bits;

            /* TODO: duplicated in registers */
            sptNnzIndex const bptr_begin = dev_bptr[b];
            sptNnzIndex const bptr_end = dev_bptr[b+1];

            /* Thread level */
            z = tidx + bptr_begin;
            if(z < bptr_end) {
                /* TODO: duplicated in R threads */
                sptValue const entry = dev_values[z];
                sptNnzIndex const mode_i = block_coord_mode + 
                  dev_einds[mode][z];
                sptNnzIndex const tmp_i_1 = block_coord_1 + 
                  dev_einds[times_mat_index_1][z];
                sptNnzIndex const tmp_i_2 = block_coord_2 + 
                  dev_einds[times_mat_index_2][z];

                sptValue tmp_val = 0;
                tmp_val = entry * times_mat_1[tmp_i_1 * stride + tidy] * 
                  times_mat_2[tmp_i_2 * stride + tidy];
                //printf("%f %f\n", entry, tmp_val);
                //printf("block %d, th.x %d, th.y %d\n, tmp_val %f, z %d, entry %f\n", blockIdx.x,
                //    tidx, tidy, tmp_val, z, entry);
                atomicAdd(&(mvals[mode_i * stride + tidy]), tmp_val);
            }   // End loop entries
        }
    }   // End loop blocks
}

/* impl_num = 03  Naive, 2-D, exchange tidx and tidy.
 * Limitation: R * blockDim.y (max_nnz) <= 1024.
 */
__global__ void spt_MTTKRPKernelRankSplitHiCOO_3D_naive(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    sptNnzIndex const all_nblocks = blength;
    const sptIndex tidx = threadIdx.x;
    const sptIndex tidy = threadIdx.y;
    sptNnzIndex z;
    sptIndex block_coord_mode, block_coord_1, block_coord_2;

    sptValue * const mvals = dev_mats[nmodes];
    sptIndex const times_mat_index_1 = dev_mats_order[1];
    sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
    sptIndex const times_mat_index_2 = dev_mats_order[2];
    sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

    sptNnzIndex num_loops_blocks = 1;
    if(all_nblocks > gridDim.x) {
        num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
    }
    for(sptNnzIndex nb=0; nb<num_loops_blocks; ++nb) {
        /* Block level */
        sptNnzIndex b = blockIdx.x + nb * gridDim.x;
        if(b < blength) {
            /* Block indices */
            block_coord_mode = dev_binds[mode][b] << sb_bits;
            block_coord_1 = dev_binds[times_mat_index_1][b] << sb_bits;
            block_coord_2 = dev_binds[times_mat_index_2][b] << sb_bits;

            /* TODO: duplicated in registers */
            sptNnzIndex const bptr_begin = dev_bptr[b];
            sptNnzIndex const bptr_end = dev_bptr[b+1];

            /* Thread level */
            z = tidy + bptr_begin;
            if(z < bptr_end) {
                sptValue const entry = dev_values[z];
                sptNnzIndex const mode_i = block_coord_mode + dev_einds[mode][z];
                sptNnzIndex const tmp_i_1 = block_coord_1 + dev_einds[times_mat_index_1][z];
                sptNnzIndex const tmp_i_2 = block_coord_2 + dev_einds[times_mat_index_2][z];

                sptValue tmp_val = 0;
                tmp_val = entry * times_mat_1[tmp_i_1 * stride + tidx] * times_mat_2[tmp_i_2 * stride + tidx];
                atomicAdd(&(mvals[mode_i * stride + tidx]), tmp_val);

            }   // End loop entries
        }
    }   // End loop blocks

}

/* impl_num = 04  Naive, 2-D, with rank blocking.
 * Limitation: max_R * blockDim.y (max_nnzb) <= 1024.
 */
__global__ void spt_MTTKRPKernelRankSplitHiCOORB_3D_naive(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    sptNnzIndex const all_nblocks = blength;
    const sptIndex tidx = threadIdx.x;
    const sptIndex tidy = threadIdx.y;
    sptNnzIndex z;
    sptIndex block_coord_mode, block_coord_1, block_coord_2;
    const sptIndex num_loops_r = R / blockDim.x;
    const sptIndex rest_loop = R - num_loops_r * blockDim.x;

    sptValue * const mvals = dev_mats[nmodes];
    sptIndex const times_mat_index_1 = dev_mats_order[1];
    sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
    sptIndex const times_mat_index_2 = dev_mats_order[2];
    sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

    sptNnzIndex num_loops_blocks = 1;
    if(all_nblocks > gridDim.x) {
        num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
    }

    for(sptNnzIndex nb=0; nb<num_loops_blocks; ++nb) {
        /* Block level */
        sptNnzIndex b = blockIdx.x + nb * gridDim.x;
        if(b < blength) {
            /* Block indices */
            block_coord_mode = dev_binds[mode][b] << sb_bits;
            block_coord_1 = dev_binds[times_mat_index_1][b] << sb_bits;
            block_coord_2 = dev_binds[times_mat_index_2][b] << sb_bits;

            /* TODO: duplicated in registers */
            sptNnzIndex const bptr_begin = dev_bptr[b];
            sptNnzIndex const bptr_end = dev_bptr[b+1];

            /* Thread level */
            z = tidy + bptr_begin;
            if(z < bptr_end) {
                sptValue const entry = dev_values[z];
                sptNnzIndex const mode_i = block_coord_mode + dev_einds[mode][z];
                sptNnzIndex const tmp_i_1 = block_coord_1 + dev_einds[times_mat_index_1][z];
                sptNnzIndex const tmp_i_2 = block_coord_2 + dev_einds[times_mat_index_2][z];

                sptIndex r;
                sptValue tmp_val = 0;
                for(sptIndex l=0; l<num_loops_r; ++l) {
                    r = tidx + l * blockDim.x;
                    tmp_val = entry * times_mat_1[tmp_i_1 * stride + r] * times_mat_2[tmp_i_2 * stride + r];
                    atomicAdd(&(mvals[mode_i * stride + r]), tmp_val);
                }

                if(rest_loop > 0 && tidx < rest_loop) {
                    r = tidx + num_loops_r * blockDim.x;
                    tmp_val = entry * times_mat_1[tmp_i_1 * stride + r] * times_mat_2[tmp_i_2 * stride + r];
                    atomicAdd(&(mvals[mode_i * stride + r]), tmp_val);
                }

            }   // End loop entries
        }
    }   // End loop blocks

}



/* impl_num = 14  Matrix Blocked, 2-D, with rank blocking.
 * Limitation: max_R * blockDim.y (max_nnz) <= 1024.
 */
__global__ void spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    sptNnzIndex const all_nblocks = blength;
    const sptIndex tidx = threadIdx.x;
    const sptIndex tidy = threadIdx.y;
    sptNnzIndex z;
    const sptIndex num_loops_r = R / blockDim.x;
    const sptIndex rest_loop = R - num_loops_r * blockDim.x;

    sptValue * const mvals = dev_mats[nmodes];
    sptIndex const times_mat_index_1 = dev_mats_order[1];
    sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
    sptIndex const times_mat_index_2 = dev_mats_order[2];
    sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

    sptNnzIndex num_loops_blocks = 1;
    if(all_nblocks > gridDim.x) {
        num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
    }

    for(sptNnzIndex nb=0; nb<num_loops_blocks; ++nb) {
        /* Block level */
        sptNnzIndex b = blockIdx.x + nb * gridDim.x;
        if(b < blength) {
            /* TODO: duplicated in registers */
            sptValue * blocked_mvals = mvals + (dev_binds[mode][b] << sb_bits) * stride;
            sptValue * blocked_times_mat_1 = times_mat_1 + (dev_binds[times_mat_index_1][b] << sb_bits) * stride;
            sptValue * blocked_times_mat_2 = times_mat_2 + (dev_binds[times_mat_index_2][b] << sb_bits) * stride;

            sptNnzIndex const bptr_begin = dev_bptr[b];
            sptNnzIndex const bptr_end = dev_bptr[b+1];

            /* Thread level */
            z = tidy + bptr_begin;
            if(z < bptr_end) {
                sptValue const entry = dev_values[z];
                sptElementIndex const mode_i = dev_einds[mode][z];
                sptElementIndex const tmp_i_1 = dev_einds[times_mat_index_1][z];
                sptElementIndex const tmp_i_2 = dev_einds[times_mat_index_2][z];

                sptValue * const bmvals_row = blocked_mvals + mode_i * stride;

                sptIndex r;
                sptValue tmp_val = 0;
                for(sptIndex l=0; l<num_loops_r; ++l) {
                    r = tidx + l * blockDim.x;
                    tmp_val = entry * blocked_times_mat_1[tmp_i_1 * stride + r] * blocked_times_mat_2[tmp_i_2 * stride + r];
                    atomicAdd(&(bmvals_row[r]), tmp_val);
                }

                if(rest_loop > 0 && tidx < rest_loop) {
                    r = tidx + num_loops_r * blockDim.x;
                    tmp_val = entry * blocked_times_mat_1[tmp_i_1 * stride + r] * blocked_times_mat_2[tmp_i_2 * stride + r];
                    atomicAdd(&(bmvals_row[r]), tmp_val);
                }

            }   // End loop entries
        }
    }   // End loop blocks

}



/* impl_num = 15  Matrix Blocked, 2-D, with rank blocking.
 * + switch according to block size
 * use shared memory for the output matrix
 * Limitation: max_R * blockDim.y (max_nnz) <= 1024.
 */
__global__ void spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked_SM(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    sptIndex const sb_size = (sptIndex) 1 << sb_bits;
    /* Data in shared memory */
    extern __shared__ sptValue mempool[];
    sptValue * sm_blocked_mvals = mempool;

    sptNnzIndex const all_nblocks = blength;
    const sptIndex tidx = threadIdx.x;
    const sptIndex tidy = threadIdx.y;
    sptNnzIndex z;
    const sptIndex num_loops_r = R / blockDim.x;
    const sptIndex rest_loop = R - num_loops_r * blockDim.x;
    sptIndex r;

    sptValue * const mvals = dev_mats[nmodes];
    sptIndex const times_mat_index_1 = dev_mats_order[1];
    sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
    sptIndex const times_mat_index_2 = dev_mats_order[2];
    sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

    sptNnzIndex num_loops_blocks = 1;
    if(all_nblocks > gridDim.x) {
        num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
    }

    for(sptNnzIndex nb=0; nb<num_loops_blocks; ++nb) {
        /* Block level */
        sptNnzIndex b = blockIdx.x + nb * gridDim.x;
        if(b < blength) {
            /* TODO: duplicated in registers */
            sptValue * blocked_mvals = mvals + (dev_binds[mode][b] << sb_bits) * stride;
            sptValue * blocked_times_mat_1 = times_mat_1 + (dev_binds[times_mat_index_1][b] << sb_bits) * stride;
            sptValue * blocked_times_mat_2 = times_mat_2 + (dev_binds[times_mat_index_2][b] << sb_bits) * stride;

            sptNnzIndex const bptr_begin = dev_bptr[b];
            sptNnzIndex const bptr_end = dev_bptr[b+1];

            /* Enough matrix reuse */
            if (bptr_end - bptr_begin > sb_size) {

                /* Load mats[nmodes] into shared memory, use R instead of stride. */
                if (tidy < sb_size) {
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        sm_blocked_mvals[tidy * R + r] = 0;
                    }
                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        sm_blocked_mvals[tidy * R + r] = 0;
                    }
                }
                __syncthreads();

                /* Thread level */
                z = tidy + bptr_begin;
                if(z < bptr_end) {
                    sptValue const entry = dev_values[z];
                    sptElementIndex const mode_i = dev_einds[mode][z];
                    sptElementIndex const tmp_i_1 = dev_einds[times_mat_index_1][z];
                    sptElementIndex const tmp_i_2 = dev_einds[times_mat_index_2][z];

                    sptValue * const bmvals_row = sm_blocked_mvals + mode_i * R;
                    sptValue * const blocked_times_mat_1_row = blocked_times_mat_1 + tmp_i_1 * stride;
                    sptValue * const blocked_times_mat_2_row = blocked_times_mat_2 + tmp_i_2 * stride;

                    sptValue tmp_val = 0;
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1_row[r] * blocked_times_mat_2_row[r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1_row[r] * blocked_times_mat_2_row[r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                }   // End loop entries

                /* Store back mats[nmodes] from shared memory */
                if (tidy < sb_size) {
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        atomicAdd( &(blocked_mvals[tidy * stride + r]),  sm_blocked_mvals[tidy * stride + r] );
                    }
                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        atomicAdd( &(blocked_mvals[tidy * stride + r]),  sm_blocked_mvals[tidy * stride + r] );
                    }
                }


            } else { /* Not enough matrix reuse */
                /* Thread level */
                z = tidy + bptr_begin;
                if(z < bptr_end) {
                    sptValue const entry = dev_values[z];
                    sptElementIndex const mode_i = dev_einds[mode][z];
                    sptElementIndex const tmp_i_1 = dev_einds[times_mat_index_1][z];
                    sptElementIndex const tmp_i_2 = dev_einds[times_mat_index_2][z];

                    sptValue * const bmvals_row = blocked_mvals + mode_i * stride;

                    sptValue tmp_val = 0;
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1[tmp_i_1 * stride + r] * blocked_times_mat_2[tmp_i_2 * stride + r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1[tmp_i_1 * stride + r] * blocked_times_mat_2[tmp_i_2 * stride + r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                }   // End loop entries
            }   // End if: block size

        }   // End if: block range
    }   // End loop blocks

}



/* impl_num = 16  Matrix Blocked, 2-D, with rank blocking. TODO: BUG EXISTS.
 * + switch according to block size
 * use shared memory for three matrices
 * Limitation: max_R * blockDim.y (max_nnz) <= 1024.
 */
__global__ void spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked_AllSM(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    sptIndex const sb_size = (sptIndex)powf(2, sb_bits);
    /* Data in shared memory */
    extern __shared__ sptValue mempool[];
    sptValue * sm_blocked_mvals = mempool;
    sptValue * sm_blocked_times_mat_1 = mempool + sb_size * R * sizeof(sptValue);
    sptValue * sm_blocked_times_mat_2 = sm_blocked_times_mat_1 + sb_size * R * sizeof(sptValue);

    sptNnzIndex const all_nblocks = blength;
    const sptIndex tidx = threadIdx.x;
    const sptIndex tidy = threadIdx.y;
    sptNnzIndex z;
    const sptIndex num_loops_r = R / blockDim.x;
    const sptIndex rest_loop = R - num_loops_r * blockDim.x;
    sptIndex r;

    sptValue * const mvals = dev_mats[nmodes];
    sptIndex const times_mat_index_1 = dev_mats_order[1];
    sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
    sptIndex const times_mat_index_2 = dev_mats_order[2];
    sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

    sptNnzIndex num_loops_blocks = 1;
    if(all_nblocks > gridDim.x) {
        num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
    }

    for(sptNnzIndex nb=0; nb<num_loops_blocks; ++nb) {
        /* Block level */
        sptNnzIndex b = blockIdx.x + nb * gridDim.x;
        if(b < blength) {
            /* TODO: duplicated in registers */
            sptValue * blocked_mvals = mvals + (dev_binds[mode][b] << sb_bits) * stride;
            sptValue * blocked_times_mat_1 = times_mat_1 + (dev_binds[times_mat_index_1][b] << sb_bits) * stride;
            sptValue * blocked_times_mat_2 = times_mat_2 + (dev_binds[times_mat_index_2][b] << sb_bits) * stride;

            sptNnzIndex const bptr_begin = dev_bptr[b];
            sptNnzIndex const bptr_end = dev_bptr[b+1];

            /* Enough matrix reuse */
            if (bptr_end - bptr_begin > sb_size) {

                /* Load mats[nmodes] into shared memory, use R instead of stride. */
                if (tidy < sb_size) {
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        sm_blocked_mvals[tidy * R + r] = 0;
                        sm_blocked_times_mat_1[tidy * R + r] = blocked_times_mat_1[tidy * stride + r];
                        sm_blocked_times_mat_2[tidy * R + r] = blocked_times_mat_2[tidy * stride + r];
                    }
                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        sm_blocked_mvals[tidy * R + r] = 0;
                        sm_blocked_times_mat_1[tidy * R + r] = blocked_times_mat_1[tidy * stride + r];
                        sm_blocked_times_mat_2[tidy * R + r] = blocked_times_mat_2[tidy * stride + r];
                    }
                }
                __syncthreads();

                /* Thread level */
                z = tidy + bptr_begin;
                if(z < bptr_end) {
                    sptValue const entry = dev_values[z];
                    sptElementIndex const mode_i = dev_einds[mode][z];
                    sptElementIndex const tmp_i_1 = dev_einds[times_mat_index_1][z];
                    sptElementIndex const tmp_i_2 = dev_einds[times_mat_index_2][z];

                    sptValue * const bmvals_row = sm_blocked_mvals + mode_i * R;
                    sptValue * const blocked_times_mat_1_row = sm_blocked_times_mat_1 + tmp_i_1 * R;
                    sptValue * const blocked_times_mat_2_row = sm_blocked_times_mat_2 + tmp_i_2 * R;

                    sptValue tmp_val = 0;
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1_row[r] * blocked_times_mat_2_row[r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1_row[r] * blocked_times_mat_2_row[r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                }   // End loop entries

                /* Store back mats[nmodes] from shared memory */
                if (tidy < sb_size) {
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        atomicAdd( &(blocked_mvals[tidy * stride + r]),  sm_blocked_mvals[tidy * R + r] );
                    }
                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        atomicAdd( &(blocked_mvals[tidy * stride + r]),  sm_blocked_mvals[tidy * R + r] );
                    }
                }


            } else { /* Not enough matrix reuse */
                /* Thread level */
                z = tidy + bptr_begin;
                if(z < bptr_end) {
                    sptValue const entry = dev_values[z];
                    sptElementIndex const mode_i = dev_einds[mode][z];
                    sptElementIndex const tmp_i_1 = dev_einds[times_mat_index_1][z];
                    sptElementIndex const tmp_i_2 = dev_einds[times_mat_index_2][z];

                    sptValue * const bmvals_row = blocked_mvals + mode_i * stride;

                    sptValue tmp_val = 0;
                    for(sptIndex l=0; l<num_loops_r; ++l) {
                        r = tidx + l * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1[tmp_i_1 * stride + r] * blocked_times_mat_2[tmp_i_2 * stride + r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                    if(rest_loop > 0 && tidx < rest_loop) {
                        r = tidx + num_loops_r * blockDim.x;
                        tmp_val = entry * blocked_times_mat_1[tmp_i_1 * stride + r] * blocked_times_mat_2[tmp_i_2 * stride + r];
                        atomicAdd(&(bmvals_row[r]), tmp_val);
                    }

                }   // End loop entries
            }   // End if: block size

        }   // End if: block range
    }   // End loop blocks

}

/* impl_num = 102  Naive, 2-D 
 */
__global__ void spt_MTTKRPKernelRankHiCOO_3D_naive_new(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
    //printf("in kernel 102\n");
    sptNnzIndex const all_nblocks = blength;
    const sptIndex tid = threadIdx.x;
    sptIndex tidx = 0;
    sptIndex tidy = 0;
    sptNnzIndex z;
    sptIndex block_coord_mode, block_coord_1, block_coord_2;

    sptValue * const mvals = dev_mats[nmodes];
    sptIndex const times_mat_index_1 = dev_mats_order[1];
    sptValue * const times_mat_1 = dev_mats[times_mat_index_1];
    sptIndex const times_mat_index_2 = dev_mats_order[2];
    sptValue * const times_mat_2 = dev_mats[times_mat_index_2];

    sptNnzIndex num_loops_blocks = 1;
    if(all_nblocks > gridDim.x) {
        num_loops_blocks = (all_nblocks + gridDim.x - 1) / gridDim.x;
    }
    for(sptNnzIndex nb=0; nb<num_loops_blocks; ++nb) {
        /* each thread block deals with a tensor block */
        sptNnzIndex b = blockIdx.x + nb * gridDim.x;
        if(b < blength) {
            /* Block indices */
            block_coord_mode = dev_binds[mode][b] << sb_bits;
            block_coord_1 = dev_binds[times_mat_index_1][b] << sb_bits;
            block_coord_2 = dev_binds[times_mat_index_2][b] << sb_bits;
            //nnz index
            sptNnzIndex const bptr_begin = dev_bptr[b];
            sptNnzIndex const bptr_end = dev_bptr[b+1];
            //get the number of nnz in this block
            sptIndex const bnnz = bptr_end - bptr_begin;
            //calc loop numbers
            //sptIndex const loop_bnnz = (bnnz + 3) >> 2;
            //Rank along tidx, nnz along tidy
            //ensure coaleased memory visit, loop over nnz
            tidx = tid >> 5;
            tidy = tid & 31;
            const sptIndex step = blockDim.x >> 5;
            /* Thread level */
            for(int i = 0; i < bnnz; i += step) {
              z = tidx + bptr_begin + i;
              if(z < bptr_end) {
                /* TODO: duplicated in R threads */
                sptValue const entry = dev_values[z];
                sptNnzIndex const mode_i = block_coord_mode + 
                  dev_einds[mode][z];
                sptNnzIndex const tmp_i_1 = block_coord_1 + 
                  dev_einds[times_mat_index_1][z];
                sptNnzIndex const tmp_i_2 = block_coord_2 + 
                  dev_einds[times_mat_index_2][z];

                sptValue tmp_val = 0;
                tmp_val = entry * times_mat_1[tmp_i_1 * stride + tidy] * 
                  times_mat_2[tmp_i_2 * stride + tidy];
                atomicAdd(&(mvals[mode_i * stride + tidy]), tmp_val);
                //mvals[mode_i * stride + tidy] += tmp_val;
              }   // End loop entries
            }
        }
    }   // End loop blocks
}




__inline__ __device__ int calc_1(sptNnzIndex n) {
  unsigned int count = 0;
  while(n != 0) {
    n = n & (n - 1);
    count++;
  }
  return count;
}

/* impl_num = 103 each thread block deal with a tensor chunk
 */
__global__ void sptMTTKRPHiCOOKernelOptimizedEinds3D(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
  //use parameter kptr_begin store #chunks
  sptNnzIndex const all_chunks = kptr_begin;
  sptIndex const mat1_index = dev_mats_order[1];
  sptIndex const mat2_index = dev_mats_order[2];
  sptValue * const mat_out = dev_mats[nmodes];
  sptValue * const mat1 = dev_mats[mat1_index];
  sptValue * const mat2 = dev_mats[mat2_index];
  //unsigned char to unsigned int
  sptIndex * const dev_opt_einds = (sptIndex *)dev_einds[0];
  sptNnzIndex chunk_start = 0;
  sptNnzIndex mask = 0xffffffffffffffff;
  //tidx demension for nnz, tidy demention for R
  sptIndex tidx = threadIdx.x >> 5;
  sptIndex tidy = threadIdx.x & 31;
  //calc how many loops needed to finish all the tensor chunks
  sptNnzIndex num_loops = 1;
  if(all_chunks > gridDim.x) {
      num_loops = (all_chunks + gridDim.x - 1) / gridDim.x;
  }
  //if(threadIdx.x == 0) {
  //  for(int ii = 0; ii < nnz; ii++) {
  //    printf("%x ", dev_opt_einds[ii]);
  //  }
  //  printf("\n");
  //}
  //loop over chunks
  for(sptNnzIndex nc = 0; nc < num_loops; ++nc) {
    /* each thread block deals with a tensor chunk */
    sptNnzIndex c = blockIdx.x + nc * gridDim.x;
    sptNnzIndex pattern = 0;
    sptNnzIndex b_offset = 0;
    sptIndex eindex = 0;
    if(c < all_chunks) {
      chunk_start = dev_cptr[c<<1];
      pattern = dev_cptr[(c << 1) + 1];
      //if(threadIdx.x == 0)
      //  printf("\nblockIdx.x %u, chunk_start %lu, pattern %lx\n", blockIdx.x, chunk_start, pattern);
      sptIndex block_m0, block_m1, block_m2;
      sptValue entry = 0;
      //load binds and einds in a coalesced way
      //first 16 threads in a warp for load
      //if(threadIdx.x == 0)
        //printf("blockDim %d\n", blockDim.x);
      //printf("my id %u, exe id %u\n", threadIdx.x, threadIdx.x >> 4 & 1);
      //if((threadIdx.x >> 4) & 1 == 0) { typical error
      //if((threadIdx.x >> 4 & 1) == 0) { right
      if(!(threadIdx.x >> 4 & 1)) {
        sptIndex pos = (threadIdx.x >> 5 << 4) + (threadIdx.x & 0xf);
        //printf("%u, %u, my id %u, pos: %u\n", threadIdx.x >> 5 << 4, threadIdx.x & 0xf, threadIdx.x, pos);
        b_offset = pattern & (mask >> (63 - pos));
        //calc how many blocks before
        b_offset = calc_1(b_offset);
        //block index
        sptNnzIndex b_id = 0;
        b_id = b_offset + chunk_start;
        //nnz index
        block_m0 = dev_binds[mode][b_id]       << sb_bits;
        block_m1 = dev_binds[mat1_index][b_id] << sb_bits;
        block_m2 = dev_binds[mat2_index][b_id] << sb_bits;
        pos += (c << 6);
        //if(threadIdx.x == 0 && blockIdx.x == 1) {
        //  printf("pos:%lu\n", chunk_start);
        //}
        if(pos < nnz) {
          eindex = dev_opt_einds[pos];
          entry = dev_values[pos];
        }
        //printf("my di %u, z %u, eindex %x, entry %f\n", threadIdx.x, pos, eindex, entry);
        //printf("b0 %u, b1 %u, b2 %u\n", block_m0, block_m1, block_m2);
      }
      else {
        //sptIndex z = (c << 6) + (threadIdx.x >> 5 << 4) + (threadIdx.x & 0xf);
        //if(z > nnz - 1)
        //  break;
        //eindex = dev_opt_einds[z];
        //entry = dev_values[z];
        //printf("my di %u, z %u, eindex %x, entry %f\n", threadIdx.x, z, eindex, entry);
      }
      /*loop over nnz in chunk
       *assume blockDim.x = 256, nnzs in a chunk is 64, R = 32 for now
       *each warp deal with a nnz each time
       */
      for(sptIndex i = 0; i < 16; i ++) {
        //broadcast value
        sptIndex gid = (c << 6) + (tidx << 4) + i;
        if(gid > nnz - 1)
          break;
        sptValue tmp_val = __shfl_sync(0xffffffff, entry,  i );
        sptIndex etmp    = __shfl_sync(0xffffffff, eindex, i );
        //if(threadIdx.x == 0 && blockIdx.x == 0) {
        //  printf("z:%u, eindex:%x, value:%f\n", gid, etmp, tmp_val);
        //}
        sptNnzIndex mode_0, mode_1, mode_2;
        //broadcast block_mode0
        mode_0 = __shfl_sync(0xffffffff, block_m0, i);
        mode_1 = __shfl_sync(0xffffffff, block_m1, i);
        mode_2 = __shfl_sync(0xffffffff, block_m2, i);
        //if(threadIdx.x == i) 
          //printf("b0 %u, b1 %u, b2 %u\n", block_m0, block_m1, block_m2);
        mode_0 += (etmp & 0xff);
        mode_1 += + ((etmp >> 8) & 0xff);
        mode_2 += + ((etmp >> 16) & 0xff);
        //if(threadIdx.x == 0) 
        //  printf("z %u, %f\n", i + tidx, tmp_val);
        //if(threadIdx.x == 0 && blockIdx.x == 1) {
        //  printf(" %8x,  m0 %4lu, m1 %4lu,  m2 %4lu,  %f\n", 
        //      etmp,  mode_0, mode_1, mode_2, tmp_val);
        //}
        //if(threadIdx.x == 0) { //typical error %u -- %lu
        //  printf("z %u, eindex %x, m0 %u, m1 %u, m2 %u, %f\n", 
        //      i + tidx, etmp, mode_0, mode_1, mode_2, tmp_val);
        //}
        //if(threadIdx.x % 32 == 0 ) {
        //  printf("warp:%u, b_offset:%lu, z:%lu, einds:%x, mo:%lu, m1:%lu m2:%lu \n",
        //      tidx, b_offset, z, dev_opt_einds[z], mode_0, mode_1, mode_2);
        //}
        //if(threadIdx.x == 32) {
        //  printf( "%f %f %f %f\n", tmp_val,  mat_out[mode_1 * stride +tidy], 
        //      mat1[mode_1 * stride +tidy], mat2[mode_2 * stride + tidy]);
        //}
        sptValue tmp_valhaha = tmp_val * mat1[mode_1 * stride + tidy] *
                  mat2[mode_2 * stride + tidy];
        //if(threadIdx.x == 0) {
        //  printf( "original %f, write %f\n", mat_out[mode_0 * stride + tidy], tmp_valhaha);
        //}
        atomicAdd(&(mat_out[mode_0 * stride + tidy]), tmp_valhaha);
        //if(threadIdx.x == 32 && mode_0 == 0 && tidy == 0) {
        //  printf( "read %f\n", mat_out[mode_0 * stride + tidy]);
        //}
      }
    }
  }
}

/* impl_num = 104 each thread block deal with a tensor chunk
 */
__global__ void spt_MTTKRPKernelRankHiCOO_3D_chunk(
  const sptIndex mode,
  const sptIndex nmodes,
  const sptNnzIndex nnz,
  const sptIndex R,
  const sptIndex stride,
  const sptElementIndex sb_bits,
  const sptElementIndex sc_bits,
  const sptNnzIndex blength,
  const sptNnzIndex kptr_begin,
  const sptNnzIndex kptr_end,
  sptIndex * const dev_ndims,
  sptNnzIndex * const dev_cptr,
  sptNnzIndex * const dev_bptr,
  sptBlockIndex ** const dev_binds,
  sptElementIndex ** const dev_einds,
  sptValue * const dev_values,
  sptIndex * const dev_mats_order,
  sptValue ** const dev_mats)
{
  //use parameter kptr_begin store #chunks
  sptNnzIndex const all_chunks = kptr_begin;
  sptIndex const mat1_index = dev_mats_order[1];
  sptIndex const mat2_index = dev_mats_order[2];
  sptValue * const mat_out = dev_mats[nmodes];
  sptValue * const mat1 = dev_mats[mat1_index];
  sptValue * const mat2 = dev_mats[mat2_index];
  sptNnzIndex chunk_start = 0;
  sptNnzIndex mask = 0xffffffffffffffff;
  //tidx demension for nnz, tidy demention for R
  sptIndex tidx = threadIdx.x >> 5;
  sptIndex tidy = threadIdx.x & 31;
  //calc how many loops needed to finish all the tensor chunks
  sptNnzIndex num_loops = 1;
  if(all_chunks > gridDim.x) {
      num_loops = (all_chunks + gridDim.x - 1) / gridDim.x;
  }
  //loop over chunks
  for(sptNnzIndex nc = 0; nc < num_loops; ++nc) {
    /* each thread block deals with a tensor chunk */
    sptNnzIndex c = blockIdx.x + nc * gridDim.x;
    sptNnzIndex pattern = 0;
    sptNnzIndex b_offset = 0;
    sptNnzIndex b_id = 0;
    if(c < all_chunks) {
      chunk_start = dev_cptr[c<<1];
      pattern = dev_cptr[(c << 1) + 1];
      /*loop over nnz in chunk
       *assume blockDim.x = 256, nnzs in a chunk is 64, R = 32 for now
       *each warp deal with a nnz each time
       */
      for(sptIndex i = 0; i < 64; i += 8) {
        //get 8 nnzs each time
        b_offset = pattern & (mask >> (63 - i - tidx));
        //calc how many blocks before
        b_offset = calc_1(b_offset);
        //block index
        b_id = b_offset + chunk_start;
        //nnz index
        sptNnzIndex z = (c << 6) + i + tidx;
        if(z > nnz - 1)
          break;
        sptIndex block_m0, block_m1, block_m2;
        block_m0 = dev_binds[mode][b_id]       << sb_bits;
        block_m1 = dev_binds[mat1_index][b_id] << sb_bits;
        block_m2 = dev_binds[mat2_index][b_id] << sb_bits;
        sptValue entry = dev_values[z];
        sptValue tmp_val = 0;
        sptNnzIndex mode_0, mode_1, mode_2;
        mode_0 = block_m0 + dev_einds[mode][z];
        mode_1 = block_m1 + dev_einds[mat1_index][z];
        mode_2 = block_m2 + dev_einds[mat2_index][z];
        //if(threadIdx.x % 32 == 0 ) {
        //  printf("warp:%u b_offset:%lu, z:%lu, mo:%lu, m1:%lu m2:%lu v:%f\n",
        //      tidx, b_offset, z, mode_0, mode_1, mode_2, entry);
        //}
        tmp_val = entry * mat1[mode_1 * stride + tidy] *
                  mat2[mode_2 * stride + tidy];
        atomicAdd(&(mat_out[mode_0 * stride + tidy]), tmp_val);
      }
    }
  }   
}


/* impl_num = 105 optimize based on 103
 */
__global__ void sptMTTKRPHiCOOKernelCombinedInds3D(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptElementIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptBlockIndex ** const dev_binds,
    sptElementIndex ** const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
  //use parameter kptr_begin store #chunks
  sptNnzIndex const all_chunks = kptr_begin;
  sptIndex const mat1_index = dev_mats_order[1];
  sptIndex const mat2_index = dev_mats_order[2];
  sptValue * const mat_out = dev_mats[nmodes];
  sptValue * const mat1 = dev_mats[mat1_index];
  sptValue * const mat2 = dev_mats[mat2_index];
  //unsigned char to unsigned int
  sptIndex * const dev_opt_einds = (sptIndex *)dev_einds[0];
  sptNnzIndex * const dev_opt_binds = (sptNnzIndex *)dev_binds;
  sptNnzIndex chunk_start = 0;
  sptNnzIndex mask = 0xffffffffffffffff;
  //tidx demension for nnz, tidy demention for R
  sptIndex tidx = threadIdx.x >> 5;
  sptIndex tidy = threadIdx.x & 31;
  //calc how many loops needed to finish all the tensor chunks
  sptNnzIndex num_loops = 1;
  if(all_chunks > gridDim.x) {
      num_loops = (all_chunks + gridDim.x - 1) / gridDim.x;
  }
  //loop over chunks
  for(sptNnzIndex nc = 0; nc < num_loops; ++nc) {
    /* each thread block deals with a tensor chunk */
    sptNnzIndex c = blockIdx.x + nc * gridDim.x;
    sptIndex eindex = 0;
    if(c < all_chunks) {
      sptNnzIndex pattern = 0;
      //low efficiency
      chunk_start = dev_cptr[c<<1];
      pattern = dev_cptr[(c << 1) + 1];

      sptIndex index_m0 = 0, index_m1 = 0, index_m2 = 0;
      sptValue entry = 0;
      //load binds and einds in a coalesced way
      //first 16 threads in a warp for load
      if(!(threadIdx.x >> 4 & 1)) {
        sptNnzIndex b_offset = 0;
        sptNnzIndex b_id = 0;
        sptIndex pos = (threadIdx.x >> 5 << 4) + (threadIdx.x & 0xf);
        b_offset = pattern & (mask >> (63 - pos));
        pos += (c << 6);
        if(pos < nnz) {
          eindex = dev_opt_einds[pos];
          entry = dev_values[pos];
        }
        //calc how many blocks before
        b_offset = calc_1(b_offset);
        //block index
        b_id = b_offset + chunk_start;
        //nnz index
        sptNnzIndex block_cur;
        block_cur = dev_opt_binds[b_id];
        index_m0 += ((eindex & ((((sptIndex)1 << sb_bits) - 1) << sb_bits * mode))
            >> sb_bits * mode);
        index_m1 += ((eindex & ((((sptIndex)1 << sb_bits) - 1) << sb_bits * mat1_index))
            >> sb_bits * mat1_index);
        index_m2 += ((eindex & ((((sptIndex)1 << sb_bits) - 1) << sb_bits * mat2_index))
            >> sb_bits * mat2_index);
        index_m0 += ((block_cur & ((sptNnzIndex)0x1fffff << 21 * mode)) >>
            21 * mode << sb_bits);
        index_m1 += ((block_cur & ((sptNnzIndex)0x1fffff << 21 * mat1_index)) >>
            21 * mat1_index << sb_bits);
        index_m2 += ((block_cur & ((sptNnzIndex)0x1fffff << 21 * mat2_index)) >>
            21 * mat2_index << sb_bits);
        //index_m0 += (eindex & (((sptIndex)1 << sb_bits) - 1));
        //eindex >>= sb_bits;
        //index_m1 += (eindex & (((sptIndex)1 << sb_bits) - 1));
        //eindex >>= sb_bits;
        //index_m2 += (eindex & (((sptIndex)1 << sb_bits) - 1));
        //index_m0 += ((block_cur & 0x1fffff) << sb_bits);
        //index_m1 += (((block_cur >> 21) & 0x1fffff) << sb_bits);
        //index_m2 += (((block_cur >> 42) & 0x1fffff) << sb_bits);
        //printf("%2u %2u %2u %f \n", index_m0, index_m1, index_m2, entry);
      }
      /*loop over nnz in chunk
       *assume blockDim.x = 256, nnzs in a chunk is 64, R = 32 for now
       *each warp deal with a nnz each time
       */
      for(sptIndex i = 0; i < 16; i ++) {
        //broadcast value
        sptIndex gid = (c << 6) + (tidx << 4) + i;
        if(gid > nnz - 1)
          break;
        sptValue tmp_val = __shfl_sync(0xffffffff, entry,  i );
        sptNnzIndex index_m0_cur, index_m1_cur, index_m2_cur;
        //broadcast 
        index_m0_cur = __shfl_sync(0xffffffff, index_m0, i);
        index_m1_cur = __shfl_sync(0xffffffff, index_m1, i);
        index_m2_cur = __shfl_sync(0xffffffff, index_m2, i);
        tmp_val = tmp_val * mat1[index_m1_cur * stride + tidy] *
                  mat2[index_m2_cur * stride + tidy];
        atomicAdd(&(mat_out[index_m0_cur * stride + tidy]), tmp_val);
        //mat_out[index_m0_cur * stride + tidy] += tmp_val;
      }
    }
  }
}


/* impl_num = 106 same with 105
 */
__global__ void sptMTTKRPHiCOOKernelCombinedInds3DGPU(
    const sptIndex mode,
    const sptIndex nmodes,
    const sptNnzIndex nnz,
    const sptIndex R,
    const sptIndex stride,
    const sptIndex sb_bits,
    const sptElementIndex sc_bits,
    const sptNnzIndex blength,
    const sptNnzIndex kptr_begin,
    const sptNnzIndex kptr_end,
    sptIndex * const dev_ndims,
    sptNnzIndex * const dev_cptr,
    sptNnzIndex * const dev_bptr,
    sptNnzIndex * const dev_binds,
    sptIndex * const dev_einds,
    sptValue * const dev_values,
    sptIndex * const dev_mats_order,
    sptValue ** const dev_mats)
{
  //use parameter kptr_begin store #chunks
  sptNnzIndex const all_chunks = kptr_begin;
  //sptIndex const mat1_index = dev_mats_order[1];
  //sptIndex const mat2_index = dev_mats_order[2];
  sptValue * const mat_out = dev_mats[nmodes];
  sptValue * const mat1 = dev_mats[(mode + 1) % nmodes];
  sptValue * const mat2 = dev_mats[(mode + 2) % nmodes];
  sptNnzIndex chunk_start = 0;
  sptNnzIndex mask = 0xffffffffffffffff;
  //tidx demension for nnz, tidy demention for R
  sptIndex tidx = threadIdx.x >> 5;
  sptIndex tidy = threadIdx.x & 31;
  //calc how many loops needed to finish all the tensor chunks
  sptNnzIndex num_loops = 1;
  if(all_chunks > gridDim.x) {
      num_loops = (all_chunks + gridDim.x - 1) / gridDim.x;
  }
  //loop over chunks
  for(sptNnzIndex nc = 0; nc < num_loops; ++nc) {
    /* each thread block deals with a tensor chunk */
    sptNnzIndex c = blockIdx.x + nc * gridDim.x;
    sptIndex eindex = 0;
    if(c < all_chunks) {
      sptNnzIndex pattern = 0;
      //low efficiency
      chunk_start = dev_cptr[c<<1];
      pattern = dev_cptr[(c << 1) + 1];
      //printf("c:%lu pattern:%lu\n", chunk_start, pattern);

      //sptIndex index_m0 = 0, index_m1 = 0, index_m2 = 0;
      sptIndex indis[3];
      sptValue entry = 0;
      //load binds and einds in a coalesced way
      //first 16 threads in a warp for load
      if(!(threadIdx.x >> 4 & 1)) {
        sptNnzIndex b_offset = 0;
        sptNnzIndex b_id = 0;
        sptIndex pos = (threadIdx.x >> 5 << 4) + (threadIdx.x & 0xf);
        b_offset = pattern & (mask >> (63 - pos));
        pos += (c << 6);
        if(pos < nnz) {
          eindex = dev_einds[pos];
          entry = dev_values[pos];
        }
        //calc how many blocks before
        b_offset = calc_1(b_offset);
        //block index
        b_id = b_offset + chunk_start;
        //nnz index
        sptNnzIndex block_cur;
        block_cur = dev_binds[b_id];
        sptIndex sb0 = sb_bits & 0xf;
        indis[0] = eindex & ((ONE_32 << sb0) - 1) ;
        indis[0] += ((block_cur & (sptNnzIndex)0x1fffff) << sb0);
        eindex >>= sb0;
        block_cur >>= 21;
        sptIndex sb1 = (sb_bits >> 4) & 0xf;
        indis[1] = eindex & ((ONE_32 << sb1) - 1);
        indis[1] += ((block_cur & (sptNnzIndex)0x1fffff) << sb1);
        eindex >>= sb1;
        block_cur >>= 21;
        sptIndex sb2 = (sb_bits >> 8) & 0xf;
        indis[2] = eindex & ((ONE_32 << sb2) - 1); 
        indis[2] += ((block_cur & (sptNnzIndex)0x1fffff) << sb2);
        //printf("%u %u %u %2u %2u %2u %f \n", sb0, sb1, sb2, 
        //    indis[0], indis[1], indis[2], entry);
      }
      /*loop over nnz in chunk
       *assume blockDim.x = 256, nnzs in a chunk is 64, R = 32 for now
       *each warp deal with a nnz each time
       */
      sptIndex gid = (c << 6) + (tidx << 4);
      for(sptIndex i = 0; i < 16; i ++) {
        //broadcast value
        if(gid > nnz - 1)
          break;
        //broadcast 
        sptNnzIndex index_m0_cur, index_m1_cur, index_m2_cur;
        if(mode == 0) {
          index_m0_cur = __shfl_sync(0xffffffff, indis[0], i);
          index_m1_cur = __shfl_sync(0xffffffff, indis[1], i);
          index_m2_cur = __shfl_sync(0xffffffff, indis[2], i);
        } else if (mode == 1) {
          index_m0_cur = __shfl_sync(0xffffffff, indis[1], i);
          index_m1_cur = __shfl_sync(0xffffffff, indis[2], i);
          index_m2_cur = __shfl_sync(0xffffffff, indis[0], i);
        }
        else {
          index_m0_cur = __shfl_sync(0xffffffff, indis[2], i);
          index_m1_cur = __shfl_sync(0xffffffff, indis[0], i);
          index_m2_cur = __shfl_sync(0xffffffff, indis[1], i);
        }
        sptValue tmp_val = __shfl_sync(0xffffffff, entry,  i );
        tmp_val = tmp_val * mat1[index_m1_cur * stride + tidy] *
                  mat2[index_m2_cur * stride + tidy];
        atomicAdd(&(mat_out[index_m0_cur * stride + tidy]), tmp_val);
        //mat_out[index_m0_cur * stride + tidy] += tmp_val;
        gid++;
      }
    }
  }
}
