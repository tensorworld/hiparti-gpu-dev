/*
    This file is part of ParTI!.

    ParTI! is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    ParTI! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with ParTI!.
    If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <ParTI.h>
#ifdef PARTI_USE_OPENMP
    #include <omp.h>
#endif
#include "../src/sptensor/hicoo/hicoo.h"

void print_usage(char ** argv) {
    printf("Usage: %s [options] \n\n", argv[0]);
    printf("Options: -i INPUT, --input=INPUT\n");
    printf("         -o OUTPUT, --output=OUTPUT\n");
    printf("         -e RENUMBER, --renumber=RENUMBER\n");
    printf("         -n NITERS_RENUM\n");
    printf("         -p IMPL_NUM, --impl-num=IMPL_NUM\n");
    printf("         -d CUDA_DEV_ID, --cuda-dev-id=DEV_ID\n");
    printf("         -r RANK\n");
    printf("         -t NTHREADS, --nt=NT\n");
    printf("         -u use_reduce, --ur=use_reduce\n");
    printf("         --help\n");
    printf("\n");
}


int main(int argc, char ** argv) {
    FILE *fi = NULL, *fo = NULL;
    sptSparseTensor X;
    sptIndex R = 16;
    sptIndex niters = 1; //5; // 50
    double tol = 1e-5;
    sptKruskalTensor ktensor;
    int cuda_dev_id = -2;
    int nthreads = 1;
    int use_reduce = 0;
    int impl_num = 0;
    int renumber = 0;
    int niters_renum = 3;
    /* renumber:
     * = 0 : no renumbering.
     * = 1 : renumber with Lexi-order
     * = 2 : renumber with BFS-like
     * = 3 : randomly renumbering, specify niters_renum.
     */

    if(argc < 2) {
        print_usage(argv);
        exit(1);
    }

    int c;
    for(;;) {
        static struct option long_options[] = {
            {"input", required_argument, 0, 'i'},
            {"output", optional_argument, 0, 'o'},
            {"impl-num", optional_argument, 0, 'p'},
            {"renumber", optional_argument, 0, 'e'},
            {"niters-renum", optional_argument, 0, 'n'},
            {"cuda-dev-id", optional_argument, 0, 'd'},
            {"rank", optional_argument, 0, 'r'},
            {"nt", optional_argument, 0, 't'},
            {"use-reduce", optional_argument, 0, 'u'},
            {"help", no_argument, 0, 0},
            {0, 0, 0, 0}
        };
        int option_index = 0;
        c = getopt_long(argc, argv, "i:o:p:e:n:d:r:t:u:", long_options, &option_index);
        if(c == -1) {
            break;
        }
        switch(c) {
        case 'i':
            fi = fopen(optarg, "r");
            sptAssert(fi != NULL);
            printf("input file: %s\n", optarg); fflush(stdout);
            break;
        case 'o':
            fo = fopen(optarg, "w");
            sptAssert(fo != NULL);
            printf("output file: %s\n", optarg); fflush(stdout);
            break;
        case 'p':
            sscanf(optarg, "%d", &impl_num);
            break;
        case 'e':
            sscanf(optarg, "%d", &renumber);
            break;
        case 'n':
            sscanf(optarg, "%d", &niters_renum);
            break;
        case 'd':
            sscanf(optarg, "%d", &cuda_dev_id);
            break;
        case 'r':
            sscanf(optarg, "%u"PARTI_SCN_INDEX, &R);
            break;
        case 'u':
            sscanf(optarg, "%d", &use_reduce);
            break;
        case 't':
            sscanf(optarg, "%d", &nthreads);
            break;
        case '?':   /* invalid option */
        case 'h':
        default:
            print_usage(argv);
            exit(1);
        }
    }
    printf("cuda_dev_id: %d\n", cuda_dev_id);
    printf("renumber: %d\n", renumber);
    if (renumber == 1)
        printf("niters_renum: %d\n\n", niters_renum);

    sptAssert(sptLoadSparseTensor(&X, 1, fi) == 0);
    fclose(fi);
    sptSparseTensorStatus(&X, stdout);
    // sptDumpSparseTensor(&X, 0, stdout);


    /* Renumber the input tensor */
    sptIndex ** map_inds;
    if (renumber > 0) {
        map_inds = (sptIndex **)malloc(X.nmodes * sizeof *map_inds);
        spt_CheckOSError(!map_inds, "MTTKRP HiCOO");
        for(sptIndex m = 0; m < X.nmodes; ++m) {
            map_inds[m] = (sptIndex *)malloc(X.ndims[m] * sizeof (sptIndex));
            spt_CheckError(!map_inds[m], "MTTKRP HiCOO", NULL);
            for(sptIndex i = 0; i < X.ndims[m]; ++i) 
                map_inds[m][i] = i;
        }

        sptTimer renumber_timer;
        sptNewTimer(&renumber_timer, 0);
        sptStartTimer(renumber_timer);

        if ( renumber == 1 || renumber == 2) { /* Set the Lexi-order or BFS-like renumbering */
            #if 0
            orderit(&X, map_inds, renumber, niters_renum);
            #else
            // Fix sb_bits = 7
            sptIndexRenumber(&X, map_inds, renumber, niters_renum, 7, nthreads, impl_num);
            #endif
            // orderforHiCOO((int)(X.nmodes), (sptIndex)X.nnz, X.ndims, X.inds, map_inds);
        }
        if ( renumber == 3) { /* Set randomly renumbering */
            printf("[Random Indexing]\n");
            sptGetRandomShuffledIndices(&X, map_inds);
        }
        // fflush(stdout);

        sptStopTimer(renumber_timer);
        sptPrintElapsedTime(renumber_timer, "Renumbering");
        sptFreeTimer(renumber_timer);

        sptTimer shuffle_timer;
        sptNewTimer(&shuffle_timer, 0);
        sptStartTimer(shuffle_timer);

        sptSparseTensorShuffleIndices(&X, map_inds);

        sptStopTimer(shuffle_timer);
        sptPrintElapsedTime(shuffle_timer, "Shuffling time");
        sptFreeTimer(shuffle_timer);
        printf("\n");

        // sptSparseTensorSortIndex(&X, 1);
        // printf("map_inds:\n");
        // for(sptIndex m = 0; m < X.nmodes; ++m) {
        //     sptDumpIndexArray(map_inds[m], X.ndims[m], stdout);
        // }
        // sptAssert(sptDumpSparseTensor(&X, 0, stdout) == 0);
    }    

    sptIndex nmodes = X.nmodes;
    sptNewKruskalTensor(&ktensor, nmodes, X.ndims, R);

    /* For warm-up caches, timing not included */
    if(cuda_dev_id == -2) {
        nthreads = 1;
        sptAssert(sptCpdAls(&X, R, niters, tol, &ktensor) == 0);
    } else if(cuda_dev_id == -1) {
        omp_set_num_threads(nthreads);
        #pragma omp parallel
        {
            nthreads = omp_get_num_threads();
        }
        printf("nthreads: %d\n", nthreads);
        printf("use_reduce: %d\n", use_reduce);
        sptAssert(sptOmpCpdAls(&X, R, niters, tol, nthreads, use_reduce, &ktensor) == 0);
    }

    // for(int it=0; it<nloops; ++it) {
    //     if(cuda_dev_id == -2) {
    //         nthreads = 1;
    //         sptAssert(sptCpdAls(&X, R, niters, tol, &ktensor) == 0);
    //     } else if(cuda_dev_id == -1) {
    //         #pragma omp parallel
    //         {
    //             nthreads = omp_get_num_threads();
    //         }
    //         printf("nthreads: %d\n", nthreads);
    //         sptAssert(sptOmpCpdAls(&X, R, niters, tol, nthreads, use_reduce, &ktensor) == 0);
    //     }
    // }

    if(fo != NULL) {
        // Dump ktensor to files
        if (renumber > 0) {
            sptKruskalTensorInverseShuffleIndices(&ktensor, map_inds);
        }
        sptAssert( sptDumpKruskalTensor(&ktensor, fo) == 0 );
        fclose(fo);
    }

    if (renumber > 0) {
        for(sptIndex m = 0; m < X.nmodes; ++m) {
            free(map_inds[m]);
        }
        free(map_inds);
    }
    sptFreeSparseTensor(&X);
    sptFreeKruskalTensor(&ktensor);

    return 0;
}
