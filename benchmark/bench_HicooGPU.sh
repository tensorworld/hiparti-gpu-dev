#!/bin/bash
declare -a tensors
declare -a kernels
declare -a block
tensors=(nell1-123.tns nell2-123.tns fr_m-123.tns fr_s-123.tns darpa-123.tns)
#tensors=(nell1-123.tns) 
modes=(1 2)
#blocks=(3 4 5 6 7 8 9 10)
#blocks=(6)
#do
#  ls -lt ~/tensors/$tensor_item
#done
#for tensor_item in ${tensors[@]}
#do
#  #ls -lt ~/tensors/$tensor_item
#  for mode in ${modes[@]}
#  do
#    for block_size in ${blocks[@]}
#    do
#    ./mttkrp -i ~/tensors/$tensor_item -m $mode -b $block_size -k 8 -c 8 -r 32 -p 105 > performance-`date +%Y%m%d%H%M%S`.log
#    done
#  done
#done
#grep "\[GFLOPS\]" performance.txt > performance-`date +%Y%m%d%H%M%S`.log
#rm performance.txt

#tensors=(nell2-123.tns darpa-123.tns)
#blocks=(9 8)
blocks=(10 9 4 5 8)
for i in {0..4}
do
  tensor_item=${tensors[$i]}
  block_size=${blocks[$i]}
  echo $tensor_item $block_size
  for mode in ${modes[@]}
  do
    echo $tensor_item $block_size $mode
    ./mttkrp -i ~/tensors/$tensor_item -m $mode -b $block_size -k 8 -c 8 -r 32 -p 105 | tee performance-$mode-`date +%Y%m%d%H%M%S`.log
  done
done

#./mttkrp -i ~/tensors/nell2-123.tns -m 0 -b 3 -k 8 -c 8 -r 32 
