/*
    This file is part of ParTI!.
ParTI! is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    ParTI! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with ParTI!.
    If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <omp.h>
#include <ParTI.h>
#include "../src/sptensor/sptensor.h"
#include "../src/sptensor/hicoo/hicoo.h"


void printBinary(sptNnzIndex n) {
  char s[65];
  char ss[72];
  s[64] = '\n';
  for(int i = 1; i < 65; i++) {
    if((n & 1) == 1)
      s[64 - i] = '1';
    else
      s[64 - i] = '0';
    n = n >> 1;
  }
  for(int i = 0; i < 8; i++) {
    for(int j = 0; j < 8; j++) {
      ss[i*9 +j] = s[i * 8 +j];
    }
    ss[i*9+8] = ' ';
  }
  ss[8 * 9 - 1] = '\n';
  fprintf(stdout, "%s\n", ss);
}

int calc_1(sptNnzIndex n) {
	int count = 0;
	while(n != 0) {
		n = n & (n - 1);
		count++;
	}
	return count;
}

int main(int argc, char ** argv) {
    FILE *fi = NULL, *fo = NULL;
    sptSparseTensor tsr;
    sptMatrix ** U;
    sptSparseTensorHiCOOGPU hitsr;
    //sptIndex sb_bits;
    unsigned int sb_bits;
    sptElementIndex sk_bits;
    sptElementIndex sc_bits;
    fi = fopen(argv[1], "r");
    sscanf(argv[2], "%x" , &sb_bits);
    sscanf(argv[3], "%" PARTI_SCN_ELEMENT_INDEX, &sk_bits);
    sscanf(argv[4], "%" PARTI_SCN_ELEMENT_INDEX, &sc_bits);
    sptElementIndex sb0 = sb_bits & 0xf;
    sptElementIndex sb1 = (sb_bits >> 4) & 0xf;
    sptElementIndex sb2 = (sb_bits >> 8) & 0xf;

    fprintf(stdout, "sb_bits:%x %u %u %u" ,sb_bits, sb0, sb1, sb2);
    fprintf(stdout, "\nsk_bits:%" PARTI_SCN_ELEMENT_INDEX, sk_bits);
    fprintf(stdout, "\nsc_bits:%" PARTI_SCN_ELEMENT_INDEX, sc_bits);
    fprintf(stdout, "\n");

    sptAssert(sptLoadSparseTensor(&tsr, 1, fi) == 0);
    // sptSparseTensorSortIndex(&tsr, 1);
    fclose(fi);
    sptSparseTensorStatus(&tsr, stdout);
    /* Convert to HiCOO tensor */
    sptNnzIndex max_nnzb = 0;
    //sptAssert(sptSparseTensorToHiCOONew3(&hitsr, &max_nnzb, &tsr, sb_bits, sk_bits, sc_bits, 1) == 0);
    fprintf(stdout, "Here start transform\n");
    sptSparseTensorToHiCOOCombinedIndsGPU(&hitsr, &max_nnzb, &tsr, sb_bits, sk_bits, sc_bits, 1);
    fprintf(stdout, "Here finish transform\n");
    sptFreeSparseTensor(&tsr);
    //for(sptIndex i = 0; i < hitsr.cptr.len; i++) {
    //  fprintf(stdout, "%lu ", hitsr.cptr.data[i]);
    //}
    //fprintf(stdout, "\n");
    fprintf(stdout, "%lx\n", hitsr.cptr.data[1]);
    printBinary(hitsr.cptr.data[1]);
    //fprintf(stdout, "%lx\n", hitsr.cptr.data[3]);
    //printBinary(hitsr.cptr.data[3]);
    //fprintf(stdout, "%lx\n", hitsr.cptr.data[5]);
    //printBinary(hitsr.cptr.data[5]);
		sptNnzIndex mask = 0xffffffffffffffff;
		for(sptNnzIndex i = 0; i < hitsr.cptr.len; i += 2) {
			sptNnzIndex chunk_begin = hitsr.cptr.data[i];
			sptNnzIndex pattern = hitsr.cptr.data[i+1];
			//each chunk has 64 elements(the last chunk may fewer than 64)
			//loop over nnzs in a chunk
			for(sptNnzIndex j = 0; j < 64; j++) {
				//nnz value
        //sptNnzIndex nnz_id = (i << 6) + j;
        //if(nnz_id > tsr.nnz - 1)
        //  break;
        //sptValue entry = hitsr.values.data[nnz_id];
				////get block index
				//sptNnzIndex tmp = pattern & (mask >> (63 - j));
				//sptIndex b_offset = calc_1(tmp);	
				//sptNnzIndex b_id = chunk_begin + b_offset;
        ////fprintf(stdout, "%lu %lu\n", chunk_begin, b_id);
        //sptNnzIndex block_m0, block_m1, block_m2;
        ////block index
        //block_m0 = hitsr.binds.data[b_id] & (sptNnzIndex)0x1ffff;
        //block_m1 = hitsr.binds.data[b_id] & ((sptNnzIndex)0x1ffff << 21);
        //block_m2 = hitsr.binds.data[b_id] & ((sptNnzIndex)0x1ffff << 42);
        ////nnz
        //sptNnzIndex mode_0, mode_1, mode_2;
        //sptIndex *tmp = (sptIndex *)hitsr.ends
        //mode_0 = block_m0 + hitsr.einds.data[(nnz_id << 2)];
        //mode_1 = block_m1 + hitsr.einds.data[(nnz_id << 2) + 1];
        //mode_2 = block_m2 + hitsr.einds.data[(nnz_id << 2) + 2];
        //fprintf(stdout, "%3lu, %3lu, %3lu, %3lu, %3lu, %3lu\n", 
        //    block_m0, block_m1, block_m2,
        //    mode_0, mode_1, mode_2);
			}
		}
    fprintf(stdout, "\n");
    for(sptIndex i = 1; i < hitsr.bptr.len; i++) {
      //fprintf(stdout, "block %u:\n", i);
      for(sptIndex j = hitsr.bptr.data[i-1]; j < hitsr.bptr.data[i]; j++) {
        sptValue entry = hitsr.values.data[j];
        //block index
        sptNnzIndex block_m0, block_m1, block_m2;
        block_m0 = hitsr.binds.data[i-1] & (sptNnzIndex)0x1fffff;
        block_m1 = (hitsr.binds.data[i-1] >> 21) & (sptNnzIndex)0x1fffff;
        block_m2 = (hitsr.binds.data[i-1] >> 42) & (sptNnzIndex)0x1fffff;
        block_m0 <<= sb0;
        block_m1 <<= sb1;
        block_m2 <<= sb2;
        //nnz
        sptNnzIndex mode_0, mode_1, mode_2;
        sptIndex e0, e1, e2;
        sptIndex eindex = hitsr.einds.data[j];
        sptIndex tmp_eindex = eindex;
        e0 = (tmp_eindex & (((sptIndex)1 << sb0) - 1));
        tmp_eindex >>= sb0;
        e1 = (tmp_eindex & (((sptIndex)1 << sb1) - 1));
        tmp_eindex >>= sb1;
        e2 = (tmp_eindex & (((sptIndex)1 << sb2) - 1));
        
        fprintf(stdout, "einds %x %3u %3u %3u, binds %3lu, %3lu, %3lu, index: %3lu %3lu %3lu %f\n", 
            eindex, e0, e1, e2, 
            block_m0, block_m1, block_m2, 
            e0 + block_m0, e1 + block_m1, e2 + block_m2,
            hitsr.values.data[j]);
      }
      printf("\n");
    }
    fprintf(stdout, "\n");
    fprintf(stdout, "sptBlockIndex:%ld Bytes\n", sizeof(sptBlockIndex) );
    fprintf(stdout, "sptElementIndex:%ld Bytes\n", sizeof(sptElementIndex) );

    return 0;
}
