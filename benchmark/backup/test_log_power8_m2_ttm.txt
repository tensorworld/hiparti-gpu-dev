Number of CPU cores (including hyperthreads): 128
Architecture:          ppc64le
Byte Order:            Little Endian
CPU(s):                128
On-line CPU(s) list:   0-127
Thread(s) per core:    8
Core(s) per socket:    8
Socket(s):             2
NUMA node(s):          2
Model:                 8335-GTA
L1d cache:             64K
L1i cache:             32K
L2 cache:              512K
L3 cache:              8192K
NUMA node0 CPU(s):     0-63
NUMA node8 CPU(s):     64-127
/usr/local/cuda/samples/1_Utilities/deviceQuery/deviceQuery Starting...

 CUDA Device Query (Runtime API) version (CUDART static linking)

Detected 4 CUDA Capable device(s)

Device 0: "Tesla K80"
  CUDA Driver Version / Runtime Version          7.5 / 7.5
  CUDA Capability Major/Minor version number:    3.7
  Total amount of global memory:                 11520 MBytes (12079136768 bytes)
  (13) Multiprocessors, (192) CUDA Cores/MP:     2496 CUDA Cores
  GPU Max Clock rate:                            824 MHz (0.82 GHz)
  Memory Clock rate:                             2505 Mhz
  Memory Bus Width:                              384-bit
  L2 Cache Size:                                 1572864 bytes
  Maximum Texture Dimension Size (x,y,z)         1D=(65536), 2D=(65536, 65536), 3D=(4096, 4096, 4096)
  Maximum Layered 1D Texture Size, (num) layers  1D=(16384), 2048 layers
  Maximum Layered 2D Texture Size, (num) layers  2D=(16384, 16384), 2048 layers
  Total amount of constant memory:               65536 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  2048
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
  Texture alignment:                             512 bytes
  Concurrent copy and kernel execution:          Yes with 2 copy engine(s)
  Run time limit on kernels:                     No
  Integrated GPU sharing Host Memory:            No
  Support host page-locked memory mapping:       Yes
  Alignment requirement for Surfaces:            Yes
  Device has ECC support:                        Enabled
  Device supports Unified Addressing (UVA):      Yes
  Device PCI Domain ID / Bus ID / location ID:   0 / 3 / 0
  Compute Mode:
     < Default (multiple host threads can use ::cudaSetDevice() with device simultaneously) >

Device 1: "Tesla K80"
  CUDA Driver Version / Runtime Version          7.5 / 7.5
  CUDA Capability Major/Minor version number:    3.7
  Total amount of global memory:                 11520 MBytes (12079136768 bytes)
  (13) Multiprocessors, (192) CUDA Cores/MP:     2496 CUDA Cores
  GPU Max Clock rate:                            824 MHz (0.82 GHz)
  Memory Clock rate:                             2505 Mhz
  Memory Bus Width:                              384-bit
  L2 Cache Size:                                 1572864 bytes
  Maximum Texture Dimension Size (x,y,z)         1D=(65536), 2D=(65536, 65536), 3D=(4096, 4096, 4096)
  Maximum Layered 1D Texture Size, (num) layers  1D=(16384), 2048 layers
  Maximum Layered 2D Texture Size, (num) layers  2D=(16384, 16384), 2048 layers
  Total amount of constant memory:               65536 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  2048
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
  Texture alignment:                             512 bytes
  Concurrent copy and kernel execution:          Yes with 2 copy engine(s)
  Run time limit on kernels:                     No
  Integrated GPU sharing Host Memory:            No
  Support host page-locked memory mapping:       Yes
  Alignment requirement for Surfaces:            Yes
  Device has ECC support:                        Enabled
  Device supports Unified Addressing (UVA):      Yes
  Device PCI Domain ID / Bus ID / location ID:   0 / 4 / 0
  Compute Mode:
     < Default (multiple host threads can use ::cudaSetDevice() with device simultaneously) >

Device 2: "Tesla K80"
  CUDA Driver Version / Runtime Version          7.5 / 7.5
  CUDA Capability Major/Minor version number:    3.7
  Total amount of global memory:                 11520 MBytes (12079136768 bytes)
  (13) Multiprocessors, (192) CUDA Cores/MP:     2496 CUDA Cores
  GPU Max Clock rate:                            824 MHz (0.82 GHz)
  Memory Clock rate:                             2505 Mhz
  Memory Bus Width:                              384-bit
  L2 Cache Size:                                 1572864 bytes
  Maximum Texture Dimension Size (x,y,z)         1D=(65536), 2D=(65536, 65536), 3D=(4096, 4096, 4096)
  Maximum Layered 1D Texture Size, (num) layers  1D=(16384), 2048 layers
  Maximum Layered 2D Texture Size, (num) layers  2D=(16384, 16384), 2048 layers
  Total amount of constant memory:               65536 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  2048
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
  Texture alignment:                             512 bytes
  Concurrent copy and kernel execution:          Yes with 2 copy engine(s)
  Run time limit on kernels:                     No
  Integrated GPU sharing Host Memory:            No
  Support host page-locked memory mapping:       Yes
  Alignment requirement for Surfaces:            Yes
  Device has ECC support:                        Enabled
  Device supports Unified Addressing (UVA):      Yes
  Device PCI Domain ID / Bus ID / location ID:   2 / 3 / 0
  Compute Mode:
     < Default (multiple host threads can use ::cudaSetDevice() with device simultaneously) >

Device 3: "Tesla K80"
  CUDA Driver Version / Runtime Version          7.5 / 7.5
  CUDA Capability Major/Minor version number:    3.7
  Total amount of global memory:                 11520 MBytes (12079136768 bytes)
  (13) Multiprocessors, (192) CUDA Cores/MP:     2496 CUDA Cores
  GPU Max Clock rate:                            824 MHz (0.82 GHz)
  Memory Clock rate:                             2505 Mhz
  Memory Bus Width:                              384-bit
  L2 Cache Size:                                 1572864 bytes
  Maximum Texture Dimension Size (x,y,z)         1D=(65536), 2D=(65536, 65536), 3D=(4096, 4096, 4096)
  Maximum Layered 1D Texture Size, (num) layers  1D=(16384), 2048 layers
  Maximum Layered 2D Texture Size, (num) layers  2D=(16384, 16384), 2048 layers
  Total amount of constant memory:               65536 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  2048
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
  Texture alignment:                             512 bytes
  Concurrent copy and kernel execution:          Yes with 2 copy engine(s)
  Run time limit on kernels:                     No
  Integrated GPU sharing Host Memory:            No
  Support host page-locked memory mapping:       Yes
  Alignment requirement for Surfaces:            Yes
  Device has ECC support:                        Enabled
  Device supports Unified Addressing (UVA):      Yes
  Device PCI Domain ID / Bus ID / location ID:   2 / 4 / 0
  Compute Mode:
     < Default (multiple host threads can use ::cudaSetDevice() with device simultaneously) >
> Peer access from Tesla K80 (GPU0) -> Tesla K80 (GPU1) : No
> Peer access from Tesla K80 (GPU0) -> Tesla K80 (GPU2) : No
> Peer access from Tesla K80 (GPU0) -> Tesla K80 (GPU3) : No
> Peer access from Tesla K80 (GPU1) -> Tesla K80 (GPU0) : No
> Peer access from Tesla K80 (GPU1) -> Tesla K80 (GPU2) : No
> Peer access from Tesla K80 (GPU1) -> Tesla K80 (GPU3) : No
> Peer access from Tesla K80 (GPU2) -> Tesla K80 (GPU0) : No
> Peer access from Tesla K80 (GPU2) -> Tesla K80 (GPU1) : No
> Peer access from Tesla K80 (GPU2) -> Tesla K80 (GPU3) : No
> Peer access from Tesla K80 (GPU3) -> Tesla K80 (GPU0) : No
> Peer access from Tesla K80 (GPU3) -> Tesla K80 (GPU1) : No
> Peer access from Tesla K80 (GPU3) -> Tesla K80 (GPU2) : No

deviceQuery, CUDA Driver = CUDART, CUDA Driver Version = 7.5, CUDA Runtime Version = 7.5, NumDevs = 4, Device0 = Tesla K80, Device1 = Tesla K80, Device2 = Tesla K80, Device3 = Tesla K80
Result = PASS

File: /nethome/jli458/BIGTENSORS/brainq.tns, mode 2
sptRandomizeMatrix(&U, 9, 16)
[CPU  SpTns * Mtx] operation took 1.767120981 s
[CPU  SpTns * Mtx] operation took 1.767714763 s
[CPU  SpTns * Mtx] operation took 1.765666024 s
[CPU  SpTns * Mtx] operation took 1.765160342 s
[CPU  SpTns * Mtx] operation took 1.765689977 s
[CPU  SpTns * Mtx] operation took 1.765063991 s

File: /nethome/jli458/BIGTENSORS/brainq.tns, mode 2
sptRandomizeMatrix(&U, 9, 16)
[OMP  SpTns * Mtx] operation took 0.165432014 s
[OMP  SpTns * Mtx] operation took 0.145122095 s
[OMP  SpTns * Mtx] operation took 0.129420782 s
[OMP  SpTns * Mtx] operation took 0.111638771 s
[OMP  SpTns * Mtx] operation took 0.101880395 s
[OMP  SpTns * Mtx] operation took 0.090187555 s

File: /nethome/jli458/BIGTENSORS/brainq.tns, mode 2, dev 1, normal kernel
sptRandomizeMatrix(&U, 9, 16)
[CUDA SpTns * Mtx] spt_TTMKernel<<<42870, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.055497921 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<42870, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.055576532 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<42870, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.055550174 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<42870, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.055560258 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<42870, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.055525730 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<42870, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.055738847 s

File: /nethome/jli458/BIGTENSORS/nell2.tns, mode 2
sptRandomizeMatrix(&U, 28818, 16)
[CPU  SpTns * Mtx] operation took 12.412799287 s
[CPU  SpTns * Mtx] operation took 12.415463368 s
[CPU  SpTns * Mtx] operation took 12.412336033 s
[CPU  SpTns * Mtx] operation took 12.413600078 s
[CPU  SpTns * Mtx] operation took 12.411571173 s
[CPU  SpTns * Mtx] operation took 12.418377729 s

File: /nethome/jli458/BIGTENSORS/nell2.tns, mode 2
sptRandomizeMatrix(&U, 28818, 16)
[OMP  SpTns * Mtx] operation took 0.750968412 s
[OMP  SpTns * Mtx] operation took 0.639736439 s
[OMP  SpTns * Mtx] operation took 0.612300887 s
[OMP  SpTns * Mtx] operation took 0.633834511 s
[OMP  SpTns * Mtx] operation took 0.622217350 s
[OMP  SpTns * Mtx] operation took 0.627929248 s

File: /nethome/jli458/BIGTENSORS/nell2.tns, mode 2, dev 1, normal kernel
sptRandomizeMatrix(&U, 28818, 16)
[CUDA SpTns * Mtx] spt_TTMKernel<<<10543, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.815816463 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<10543, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.680386078 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<10543, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.680318456 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<10543, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.681112046 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<10543, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.680868400 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<10543, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.681077864 s

File: /nethome/jli458/BIGTENSORS/nell1.tns, mode 2
sptRandomizeMatrix(&U, 25495389, 16)
[CPU  SpTns * Mtx] operation took 33.701101765 s
[CPU  SpTns * Mtx] operation took 33.678513010 s
[CPU  SpTns * Mtx] operation took 33.703835233 s
[CPU  SpTns * Mtx] operation took 33.697376276 s
[CPU  SpTns * Mtx] operation took 33.968671359 s
[CPU  SpTns * Mtx] operation took 34.044019441 s

File: /nethome/jli458/BIGTENSORS/nell1.tns, mode 2
sptRandomizeMatrix(&U, 25495389, 16)
[OMP  SpTns * Mtx] operation took 1.433950380 s
[OMP  SpTns * Mtx] operation took 1.262943613 s
[OMP  SpTns * Mtx] operation took 1.271273435 s
[OMP  SpTns * Mtx] operation took 1.331722634 s
[OMP  SpTns * Mtx] operation took 1.301755000 s
[OMP  SpTns * Mtx] operation took 1.280745233 s

File: /nethome/jli458/BIGTENSORS/nell1.tns, mode 2, dev 1, normal kernel
sptRandomizeMatrix(&U, 25495389, 16)
[CUDA SpTns * Mtx] spt_TTMKernel<<<542889, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 1.467948270 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<542889, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 1.388848842 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<542889, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 1.421017053 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<542889, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 1.390075484 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<542889, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 1.391390745 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<542889, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 1.392805106 s

File: /nethome/jli458/BIGTENSORS/delicious.tns, mode 2
sptRandomizeMatrix(&U, 2480308, 16)
[CPU  SpTns * Mtx] operation took 24.790814004 s
[CPU  SpTns * Mtx] operation took 24.754730814 s
[CPU  SpTns * Mtx] operation took 24.753612534 s
[CPU  SpTns * Mtx] operation took 24.809282497 s
[CPU  SpTns * Mtx] operation took 24.745313345 s
[CPU  SpTns * Mtx] operation took 24.809542987 s

File: /nethome/jli458/BIGTENSORS/delicious.tns, mode 2
sptRandomizeMatrix(&U, 2480308, 16)
[OMP  SpTns * Mtx] operation took 1.433974426 s
[OMP  SpTns * Mtx] operation took 1.070102280 s
[OMP  SpTns * Mtx] operation took 1.056326648 s
[OMP  SpTns * Mtx] operation took 1.197683653 s
[OMP  SpTns * Mtx] operation took 1.058339645 s
[OMP  SpTns * Mtx] operation took 1.137641852 s

File: /nethome/jli458/BIGTENSORS/delicious.tns, mode 2, dev 1, normal kernel
sptRandomizeMatrix(&U, 2480308, 16)
[CUDA SpTns * Mtx] spt_TTMKernel<<<1476796, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.911708502 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<1476796, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.762843909 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<1476796, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.763070327 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<1476796, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.763313919 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<1476796, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.763558614 s
[CUDA SpTns * Mtx] spt_TTMKernel<<<1476796, (32, 16), 2048>>>
[CUDA SpTns * Mtx] operation took 0.857125253 s
