/*
    This file is part of ParTI!.

    ParTI! is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    ParTI! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with ParTI!.
    If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ParTI.h>
#include "../src/sptensor/sptensor.h"

int main(int argc, char const *argv[]) {
    FILE *fX, *fY;
    sptSparseTensor X, spY;
    sptSemiSparseTensor Y;
    sptMatrix U;
    sptIndex mode = 0;
    sptIndex R = 16;
    int dev_id = -2;
    int niters = 5;

    if(argc < 5) {
        printf("Usage: %s X mode renumber [dev_id, R, Y]\n\n", argv[0]);
        return 1;
    }

    fX = fopen(argv[1], "r");
    sptAssert(fX != NULL);
    sptAssert(sptLoadSparseTensor(&X, 1, fX) == 0);
    fclose(fX);

    sscanf(argv[2], "%"PARTI_SCN_INDEX, &mode);
    int renumber = 0;
    int niters_renum = 5;
    sptElementIndex sb_bits = 7;
    /* renumber:
     * = 0 : no renumbering.
     * = 1 : renumber with Lexi-order
     * = 2 : renumber with BFS-like
     * = 3 : randomly renumbering, specify niters_renum.
     */
    sscanf(argv[3], "%d", &renumber);
    printf("renumber: %d\n", renumber);
    if (renumber == 1)
        printf("niters_renum: %d\n\n", niters_renum);   

    if(argc > 4) {
        sscanf(argv[4], "%d", &dev_id);
    }
    if(argc > 5) {
        sscanf(argv[5], "%"PARTI_SCN_INDEX, &R);
    }

    /* Renumber the input tensor */
    sptIndex ** map_inds;
    if (renumber > 0) {
        map_inds = (sptIndex **)malloc(X.nmodes * sizeof *map_inds);
        spt_CheckOSError(!map_inds, "MTTKRP HiCOO");
        for(sptIndex m = 0; m < X.nmodes; ++m) {
            map_inds[m] = (sptIndex *)malloc(X.ndims[m] * sizeof (sptIndex));
            spt_CheckError(!map_inds[m], "MTTKRP HiCOO", NULL);
            for(sptIndex i = 0; i < X.ndims[m]; ++i) 
                map_inds[m][i] = i;
        }

        sptTimer renumber_timer;
        sptNewTimer(&renumber_timer, 0);
        sptStartTimer(renumber_timer);

        if ( renumber == 1 || renumber == 2) { /* Set the Lexi-order or BFS-like renumbering */
            sptIndexRenumber(&X, map_inds, renumber, niters_renum, sb_bits, 1, 1);
        }
        if ( renumber == 3) { /* Set randomly renumbering */
            printf("[Random Indexing]\n");
            sptGetRandomShuffledIndices(&X, map_inds);
        }
        // fflush(stdout);

        sptStopTimer(renumber_timer);
        sptPrintElapsedTime(renumber_timer, "Renumbering");
        sptFreeTimer(renumber_timer);

        sptTimer shuffle_timer;
        sptNewTimer(&shuffle_timer, 0);
        sptStartTimer(shuffle_timer);

        sptSparseTensorShuffleIndices(&X, map_inds);

        sptStopTimer(shuffle_timer);
        sptPrintElapsedTime(shuffle_timer, "Shuffling time");
        sptFreeTimer(shuffle_timer);
        printf("\n");

        // sptSparseTensorSortIndex(&X, 1, 1);
        // printf("map_inds:\n");
        // for(sptIndex m = 0; m < X.nmodes; ++m) {
        //     sptDumpIndexArray(map_inds[m], X.ndims[m], stdout);
        // }
        // sptAssert(sptDumpSparseTensor(&X, 0, stdout) == 0);
    }


    fprintf(stderr, "sptRandomizeMatrix(&U, %"PARTI_PRI_INDEX ", %"PARTI_PRI_INDEX ")\n", X.ndims[mode], R);
    // sptAssert(sptRandomizeMatrix(&U, X.ndims[mode], R) == 0);
    sptAssert(sptNewMatrix(&U, X.ndims[mode], R) == 0);
    sptAssert(sptConstantMatrix(&U, 1) == 0);

    /* For warm-up caches, timing not included */
    if(dev_id == -2) {
        sptAssert(sptSparseTensorMulMatrix(&Y, &X, &U, mode) == 0);
    } else if(dev_id == -1) {
        sptAssert(sptOmpSparseTensorMulMatrix(&Y, &X, &U, mode) == 0);
    }

    for(int it=0; it<niters; ++it) {
        sptFreeSemiSparseTensor(&Y);
        if(dev_id == -2) {
            sptAssert(sptSparseTensorMulMatrix(&Y, &X, &U, mode) == 0);
        } else if(dev_id == -1) {
            sptAssert(sptOmpSparseTensorMulMatrix(&Y, &X, &U, mode) == 0);
        }
    }


    if(argc > 7) {
        sptAssert(sptSemiSparseTensorToSparseTensor(&spY, &Y, 1e-9) == 0);

        fY = fopen(argv[7], "w");
        sptAssert(fY != NULL);
        sptAssert(sptDumpSparseTensor(&spY, 0, fY) == 0);
        fclose(fY);

        sptFreeSparseTensor(&spY);
    }

    if (renumber > 0) {
        for(sptIndex m = 0; m < X.nmodes; ++m) {
            free(map_inds[m]);
        }
        free(map_inds);
    }
    sptFreeSemiSparseTensor(&Y);
    sptFreeMatrix(&U);
    sptFreeSparseTensor(&X);

    return 0;
}
