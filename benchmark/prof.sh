#!/bin/bash
kernel_names=()
kernel_names[1]=spt_MTTKRPKernelHiCOO_3D_naive 
kernel_names[2]=spt_MTTKRPKernelRankHiCOO_3D_naive
kernel_names[3]=spt_MTTKRPKernelRankSplitHiCOO_3D_naive
kernel_names[4]=spt_MTTKRPKernelRankSplitHiCOORB_3D_naive 
kernel_names[14]=spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked 
kernel_names[15]=spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked_SM 
kernel_names[16]=spt_MTTKRPKernelRankSplitHiCOORB_3D_MatrixBlocked_AllSM

log_file=prof-${kernel_names[$3]}-`date +%Y%m%d%H%M%S`

echo "sudo -E LD_LIBRARY_PATH=$LD_LIBRARY_PATH nvprof --log-file $log_file --kernels ${kernel_names[$3]} --metrics all ./profiling -i $1 -b $2 -k 16 -c 16 -p $3 -m $4 -r $5"
sudo -E LD_LIBRARY_PATH=$LD_LIBRARY_PATH nvprof --log-file $log_file --kernels ${kernel_names[$3]} --metrics all ./profiling -i $1 -b $2 -k 16 -c 16 -p $3 -m $4 -r $5
