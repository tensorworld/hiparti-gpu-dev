/*
    This file is part of ParTI!.

    ParTI! is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    ParTI! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with ParTI!.
    If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <omp.h>
#include <ParTI.h>
#include "../src/sptensor/sptensor.h"
#include "../src/sptensor/hicoo/hicoo.h"
const sptValue EPSILON = 1E-1;
bool equal(sptValue x, sptValue y){
  sptValue ref_diff = x - y;
  if((x > 1) || (-x > 1))
    ref_diff = ref_diff / x;
  return ((ref_diff < EPSILON) && (-ref_diff < EPSILON));
}
void print_usage(char ** argv) {
  printf("Usage: %s [options] \n", argv[0]);
  printf("Options: -i INPUT, --input=INPUT\n");
  printf("         -o OUTPUT, --output=OUTPUT\n");
  printf("         -b BLOCKSIZE (bits), --blocksize=BLOCKSIZE (bits)\n");
  printf("         -k KERNELSIZE (bits), --kernelsize=KERNELSIZE (bits)\n");
  printf("         -c CHUNKSIZE (bits), --chunksize=CHUNKSIZE (bits, <=9)\n");
  printf("         -e RENUMBER, --renumber=RENUMBER\n");
  printf("         -m MODE, --mode=MODE\n");
  printf("         -p IMPL_NUM, --impl-num=IMPL_NUM\n");
  printf("         -d CUDA_DEV_ID, --cuda-dev-id=DEV_ID\n");
  printf("         -r RANK\n");
  printf("         -t TK, --tk=TK\n");
  printf("         -l TB, --tb=TB\n");
  printf("\n");
}

int main(int argc, char ** argv) {
  FILE *fi = NULL, *fo = NULL;
  sptSparseTensor tsr;
  sptMatrix ** U;
  sptMatrix ** U_CPU;
  sptSparseTensorHiCOOGPU hitsr;
  sptIndex sb_bits;
  sptElementIndex sk_bits;
  sptElementIndex sc_bits;

  sptIndex mode = 0;
  sptIndex R = 32;
  int cuda_dev_id = 0;
  int impl_num = 1;
  int renumber = 0;
  int tk = 1;
  int tb = 1;

  if(argc <= 3) { // #Required arguments
      print_usage(argv);
      exit(1);
  }

  for(;;) {
      static struct option long_options[] = {
          {"input", required_argument, 0, 'i'},
          {"output", required_argument, 0, 'o'},
          {"bs", required_argument, 0, 'b'},
          {"ks", required_argument, 0, 'k'},
          {"cs", required_argument, 0, 'c'},
          {"mode", required_argument, 0, 'm'},
          {"impl-num", optional_argument, 0, 'p'},
          {"renumber", optional_argument, 0, 'e'},
          {"kernel-id", optional_argument, 0, 'd'},
          {"rank", optional_argument, 0, 'r'},
          {"tk", optional_argument, 0, 't'},
          {"tb", optional_argument, 0, 'l'},
          {0, 0, 0, 0}
      };
      int option_index = 0;
      int c = 0;
      c = getopt_long(argc, argv, "i:o:b:k:c:m:p:e:d:r:t:l:", long_options, 
          &option_index);
      if(c == -1) {
          break;
      }
      switch(c) {
      case 'i':
          fi = fopen(optarg, "r");
          sptAssert(fi != NULL);
          break;
      case 'o':
          fo = fopen(optarg, "w");
          sptAssert(fo != NULL);
          break;
      case 'b':
          sscanf(optarg, "%x" , &sb_bits);
          break;
      case 'k':
          sscanf(optarg, "%" PARTI_SCN_ELEMENT_INDEX, &sk_bits);
          break;
      case 'c':
          sscanf(optarg, "%" PARTI_SCN_ELEMENT_INDEX, &sc_bits);
          break;
      case 'm':
          sscanf(optarg, "%" PARTI_SCN_INDEX, &mode);
          break;
      case 'p':
          sscanf(optarg, "%d", &impl_num);
          break;
      case 'e':
          sscanf(optarg, "%d", &renumber);
          break;
      case 'd':
          sscanf(optarg, "%d", &impl_num);
          break;
      case 'r':
          sscanf(optarg, "%" PARTI_SCN_INDEX, &R);
          break;
      case 't':
          sscanf(optarg, "%d", &tk);
          break;
      case 'l':
          sscanf(optarg, "%d", &tb);
          break;
      case '?':   /* invalid option */
      case 'h':
      default:
          print_usage(argv);
          exit(1);
      }
  }
  sptIndex sb0 = sb_bits & 0xf;
  sptIndex sb1 = (sb_bits >> 4) & 0xf;
  sptIndex sb2 = (sb_bits >> 8) & 0xf;
  if(sb0 + sb1 + sb2 > 32) {
    printf("sbs: %u %u %u, exceed limit 32bits!\n", sb0, sb1, sb2);
    return 0;
  }

  sptAssert(sptLoadSparseTensor(&tsr, 1, fi) == 0);
  fclose(fi);
  /* Convert to HiCOO tensor */
  sptNnzIndex max_nnzb = 0;
  //sptAssert(sptSparseTensorToHiCOONew3(&hitsr, &max_nnzb, &tsr, sb_bits, sk_bits,
  // sc_bits, 1) == 0);
  sptAssert(sptSparseTensorToHiCOOCombinedIndsGPU(&hitsr, &max_nnzb, &tsr, sb_bits, sk_bits,
    sc_bits, 1) == 0);
  //sptAssert(sptSparseTensorToHiCOO(&hitsr, &max_nnzb, &tsr, sb_bits, sk_bits,
  //  sc_bits, 1) == 0);

  sptIndex nmodes = hitsr.nmodes;
  U = (sptMatrix **)malloc((nmodes+1) * sizeof(sptMatrix*));
  U_CPU = (sptMatrix **)malloc((nmodes+1) * sizeof(sptMatrix*));
  for(sptIndex m=0; m<nmodes+1; ++m) {
    U[m] = (sptMatrix *)malloc(sizeof(sptMatrix));
    U_CPU[m] = (sptMatrix *)malloc(sizeof(sptMatrix));
  } 
  sptIndex max_ndims = 0;
  for(sptIndex m=0; m<nmodes; ++m) {
    sptAssert(sptNewMatrix(U[m], hitsr.ndims[m], R) == 0);
    sptAssert(sptNewMatrix(U_CPU[m], hitsr.ndims[m], R) == 0);
    sptAssert(sptRandomizeMatrix(U[m], hitsr.ndims[m], R) == 0);
    //for(sptIndex ii  = 0; ii < hitsr.ndims[m]; ii++) {
    //  for(sptIndex jj = 0; jj < R; jj++) {
    //    U[m]->values[U[m]->stride * ii + jj] = ii + 1;
    //  }
    //}
    sptAssert(sptCopyMatrix(U_CPU[m], U[m]) == 0);
    if(hitsr.ndims[m] > max_ndims)
      max_ndims = hitsr.ndims[m];
  }
  sptAssert(sptNewMatrix(U[nmodes], max_ndims, R) == 0);
  sptAssert(sptNewMatrix(U_CPU[nmodes], max_ndims, R) == 0);
  sptAssert(sptConstantMatrix(U[nmodes], 0) == 0);
  sptAssert(sptCopyMatrix(U_CPU[nmodes], U[nmodes]) == 0);

  sptIndex * mats_order = (sptIndex*)malloc(nmodes * sizeof(*mats_order));
  mats_order[0] = mode;
  for(sptIndex i=1; i<nmodes; ++i) {
    mats_order[i] = (mode+i) % nmodes;
  }
  sptDumpIndexArray(mats_order, nmodes, stdout);

  sptCudaSetDevice(cuda_dev_id);
  /* COO MTTKRP */
  printf("COO MTTKRP... \n");
  sptAssert(sptCudaMTTKRPOneKernel(&tsr, U_CPU, mats_order, mode, 15) == 0);
  printf("COO MTTKRP finished \n");
  /* HiCOO MTTKRP */
  printf("HiCOO MTTKRP... \n");
  sptAssert(sptCudaMTTKRPHiCOOGPU(&hitsr, U, mats_order, mode, max_nnzb, 
    impl_num) == 0);
  printf("HiCOO MTTKRP finished \n");
  /* check HiCOO format, COO format as refference */
  sptMatrix *ref = U_CPU[nmodes];
  sptMatrix *check = U[nmodes];
  sptIndex stride = ref->stride;
  for(sptIndex i=0; i<ref->nrows; ++i) {
    for(sptIndex j=0; j<ref->ncols; ++j) {
      if(!equal(check->values[i * stride + j],ref->values[i * stride +j])) {
        printf("HiCOO MTTKRP results check wrong!\n");
        printf("i,%d, j, %d, %f, %f\n", i, j, 
          check->values[i * stride + j],ref->values[i * stride +j]);
        return -1;
      }
    }
  }

  printf("HiCOO MTTKRP results check passed!\n");
  if(fo != NULL) {
      sptAssert(sptDumpMatrix(U[nmodes], fo) == 0);
      fclose(fo);
  }

  /* free resources */
  sptFreeSparseTensor(&tsr);
  for(sptIndex m=0; m<nmodes; ++m) {
      sptFreeMatrix(U[m]);
      sptFreeMatrix(U_CPU[m]);
  }
  free(mats_order);
  sptFreeMatrix(U[nmodes]);
  sptFreeMatrix(U_CPU[nmodes]);
  free(U);
  free(U_CPU);
  //sptFreeSparseTensorHiCOO(&hitsr);

  return 0;
}

