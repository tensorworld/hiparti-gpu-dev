#!/usr/bin/python

import sys 

# intput_path = '../timing-results/pastel/hicoo/renumber/uint8-single-1217/'
intput_path = '../timing-results/parti/hicoo/uint8-single-0924/'
s3tsrs = ['vast-2015-mc1', 'nell2', 'choa700k', '1998DARPA', 'freebase_music', 'freebase_sampled', 'flickr', 'delicious', 'nell1']
l3tsrs = ['amazon-reviews', 'patents', 'reddit-2015']
s4tsrs = ['chicago-crime-comm-4d', 'uber-4d', 'nips-4d', 'enron-4d', 'flickr-4d', 'delicious-4d']
test_tsrs = ['flickr-4d']

r = 16
sc = 14

# input parameters
sb = 7
sk = 20

renumber = sys.argv[1]
tk = sys.argv[2]
if (renumber == '1'):
	niters_reorder = sys.argv[3]

out_str_alphab = 'hicoo-alphab.out'
out_str_cb = 'hicoo-cb.out'
print("output file: " + "\"" + out_str_alphab + "\"")
fo_alphab = open(out_str_alphab, 'w')
print("output file: " + "\"" + out_str_cb + "\"")
fo_cb = open(out_str_cb, 'w')

for tsr in s3tsrs:
	sum_seq = 0

	## sequential hicoo
	if (tk == '1'):
		if (renumber == '0'):
			suffix_fname = '-seq.txt'
			input_str = intput_path + tsr + '-b' + str(sb) + '-k' + str(sk) + '-c' + str(sc) + '-r' + str(r) + '-e' + str(renumber) + suffix_fname
			# input_str = intput_path + tsr + '-b' + str(sb) + '-k' + str(sk) + '-c' + str(sc) + '-r' + str(r) + '-e' + str(renumber) + '-renumber-seq.txt'
		elif (renumber == '1'):
			suffix_fname = '-mattile-seq.txt'
			input_str = intput_path + tsr + '-b' + str(sb) + '-k' + str(sk) + '-c' + str(sc) + '-r' + str(r) + '-e' + str(renumber) + "-n" + str(niters_reorder) + suffix_fname
	else:
		suffix_fname = '-mattile-parsort.txt'
		if(tsr == 'vast-2015-mc1'):
			sk = 8
		elif(tsr == 'nell2'):
			sk = 9
		elif(tsr == 'choa700k'):
			sk = 10
		elif(tsr == '1998DARPA'):
			sk = 15
		elif(tsr == 'freebase_music' or tsr == 'freebase_sampled'):
			sk = 16
		elif(tsr == 'flickr'):
			sk = 13
		elif(tsr == 'delicious'):
			sk = 16
		elif(tsr == 'nell1'):
			sk = 18
		# 4-D
		elif(tsr == 'chicago-crime-comm-4d' or tsr == 'uber-4d'):
			sk = 4
		elif(tsr == 'nips-4d'):
			sk = 7
		elif(tsr == 'enron-4d'):
			sk = 8
		elif(tsr == 'flickr-4d'):
			sk = 15
		elif(tsr == 'delicious-4d'):
			sk = 16

		if(sk >= 7):
			sb = 7
		else:
			sb = sk


		# input_str = intput_path + tsr + '-b' + str(sb) + '-k' + str(sk) + '-c' + str(sc) + '-r' + str(r) + '-e' + str(renumber) + '-n' + str(niters_reorder) + '-tk' + str(tk) + '-tb1' + suffix_fname
		input_str = intput_path + tsr + '-b' + str(sb) + '-k' + str(sk) + '-c' + str(sc) + '-r' + str(r) + '-tk' + str(tk) + '-tb1' + '-e' + str(renumber) + '-n' + str(niters_reorder) + suffix_fname

	# print(input_str)

	fi = open(input_str, 'r')
	for line in fi:
		line_array = line.rstrip().split(" ")
		if(len(line_array) < 2):
			continue;
		elif(line_array[0] == 'alpha_b:'):
			alpha_b = line_array[1]
			fo_alphab.write(alpha_b+'\n')
		elif(len(line_array) == 7):
			# print line_array
			if(line_array[3] == 'geometric'):
				c_b = line_array[-1]
				fo_cb.write(c_b+'\n')

	fi.close()

fo_alphab.close()
fo_cb.close()





