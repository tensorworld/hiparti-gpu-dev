#!/usr/bin/python

import sys 

# KNL
# intput_path = '../timing-results/parti/hicoo/uint8-single-knl/'
intput_path = '../prof-results/hiparti/balanced/'
s3tsrs = ['vast-2015-mc1', 'nell2', 'choa700k', '1998DARPA', 'freebase_music', 'freebase_sampled', 'flickr', 'delicious', 'nell1']
# s3tsrs = ['nell2', 'choa700k', '1998DARPA', 'freebase_music', 'freebase_sampled', 'delicious', 'nell1']
l3tsrs = ['amazon-reviews', 'patents', 'reddit-2015']
s4tsrs = ['chicago-crime-comm-4d', 'uber-4d', 'nips-4d', 'enron-4d', 'flickr-4d', 'delicious-4d']
# s4tsrs = ['chicago-crime-comm-4d', 'nips-4d', 'enron-4d', 'flickr-4d', 'delicious-4d']
test_tsrs = ['vast-2015-mc1']

r = 16
tb = 1
sc = 14
impl_num = 2
balanced=0

# input parameters
tk = sys.argv[1]
renum = int(sys.argv[2])
niters_renum = int(sys.argv[3])


# out_str = 'parti-hicoo-uint8-sb' + str(sb) + '-sk' + str(sk) + '-tk' + str(tk) + '.out'
out_str = 'parti-hicoo-uint8.out'
print("output file: " + "\"" + out_str + "\"")
fo = open(out_str, 'w')

for tsr in test_tsrs:

	if (tk != '1'):

		# Set optimal sk
		if(tsr == 'vast-2015-mc1'):
			sk = 8
		elif(tsr == 'nell2'):
			sk = 9
		elif(tsr == 'choa700k'):
			sk = 10
		elif(tsr == '1998DARPA'):
			sk = 15
		elif(tsr == 'freebase_music' or tsr == 'freebase_sampled'):
			sk = 16
		elif(tsr == 'flickr'):
			sk = 13
		elif(tsr == 'delicious'):
			sk = 16
		elif(tsr == 'nell1'):
			sk = 18
		# 4-D
		elif(tsr == 'chicago-crime-comm-4d' or tsr == 'uber-4d'):
			sk = 4
		elif(tsr == 'nips-4d'):
			sk = 7
		elif(tsr == 'enron-4d'):
			sk = 8
		elif(tsr == 'flickr-4d'):
			sk = 15
		elif(tsr == 'delicious-4d'):
			sk = 15

		if(sk >= 7):
			sb = 7
		else:
			sb = sk
			
		if (renum == 0):
			input_str = intput_path + tsr + '-b' + str(sb) + '-k' + str(sk) + '-c' + str(sc) + '-r' + str(r) + '-tk' + str(tk) + '-tb' + str(tb) + '-a' + str(balanced) + '.txt'
		else:
			input_str = intput_path + tsr + '-b' + str(sb) + '-k' + str(sk) + '-c' + str(sc) + '-r' + str(r) + '-e' + str(renum) + '-n' + str(niters_renum) + '-p' + str(impl_num) + '-tk' + str(tk) + '-tb' + str(tb) + '-a' + str(balanced) + '.txt'
	# print(input_str)

	sum_max_avg = 0.0
	total_sum_max_avg = 0.0
	count = 0
	nmodes = 0
	fi = open(input_str, 'r')
	for line in fi:
		line_array = line.rstrip().split(" ")
		# print(line_array)
		if(line_array[0] == '[CPU'):
			# print("sum_max_avg: " + str(sum_max_avg))
			avg_max_avg = sum_max_avg / count
			count = 0
			nmodes = nmodes + 1
			sum_max_avg = 0
			print(str(avg_max_avg))
			fo.write(str(avg_max_avg)+'\n')
			total_sum_max_avg += avg_max_avg

		if(len(line_array) < 8):
			continue;
		elif(line_array[2] == 'max_nnzs:'):
			count = count + 1
			word = line_array[3]
			word_array = word.rstrip().split(",")
			max_nnzs = word_array[0]
			word = line_array[5]
			word_array = word.rstrip().split(",")
			avg_nnzs = word_array[0]
			# print("max_nnzs: "+str(max_nnzs)+", avg_nnzs: "+str(avg_nnzs))
			ratio_max_avg = float(max_nnzs) / float(avg_nnzs)
			# print("ratio_max_avg: "+str(ratio_max_avg))
			sum_max_avg += ratio_max_avg

	total_avg_max_avg = total_sum_max_avg / nmodes
	print(str(total_avg_max_avg))
	fo.write(str(total_avg_max_avg)+'\n')
	total_avg_max_avg = 0
	fi.close()

fo.close()

